<?php

/*
 *   This file is part of NOALYSS.
 *
 *   Noalyss is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Noalyss is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Noalyss; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2018) Author Dany De Bontridder <dany@alchimerys.be>

/**
 * @file
 * @brief 
 */

/**
 * @brief
 */
class V_Ledger_Code_Manage_SQL extends Table_Data_SQL
{

    function __construct(Database $p_cn, $p_id=-1)
    {
        $this->table="impacc2.v_ledger_code_manage";
        $this->primary_key="id";
        /*
         * List of columns
         */
        $this->name=array(
            "id"=>"id"
            , "jrn_def_id"=>"jrn_def_id"
            , "ledger_code"=>"ledger_code"
            , "jrn_def_name"=>"jrn_def_name"
        );
        /*
         * Type of columns
         */
        $this->type=array(
            "id"=>"numeric"
            , "jrn_def_id"=>"numeric"
            , "ledger_code"=>"text"
            , "jrn_def_name"=>"text"
        );


        $this->default=array(
            "id"=>"auto"
        );

        $this->date_format="DD.MM.YYYY";
        parent::__construct($p_cn, $p_id);
    }

    function insert()
    {
        $id=$this->cn->get_value("insert into impacc2.ledger_code(jrn_def_id,ledger_code) 
                values ($1,$2) 
                returning id;",[
                    $this->jrn_def_id,
                    $this->ledger_code
                ]);
        $this->setp("id",$id);
        $this->load();
    }

    function update()
    {
        $this->cn->exec_sql("update impacc2.ledger_code set ledger_code=$1 , jrn_def_id=$2 where id=$3",
                [
                    $this->ledger_code,
                    $this->jrn_def_id,
                    $this->id
                ]);
    }

    function delete()
    {
        $this->cn->exec_sql("delete from impacc2.ledger_code where id=$1",[$this->id]);
    }

   

}
