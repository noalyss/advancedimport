<?php
namespace NoalyssImport;
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/**
 * @file
 * @brief
 *
 */
class Install_Plugin
{
    function __construct()
    {
        
    }

    function install($p_cn)
    {
        global $g_impacc;
        try
        {
            $p_cn->start();
            $p_cn->execute_script($g_impacc."/sql/install.sql");

            $p_cn->commit();
        }
        catch (\Exception $e)
        {
            $p_cn->rollback();
            echo _('ERREUR')." Install_Impacc::install";
            record_log($e->getTraceAsString());
            return -1;
        }
    }

    /***
     * @brief return the current schema version
     */
    function get_current_version()
    {
        $cn=\Dossier::connect();
        $current=$cn->get_value("select max(v_id) from impacc2.version");
        return $current;
    }
    /***
     * @brief upgrade the schema to the last version
     */
    function upgrade()
    {
        global $g_impacc;
        $cn=\Dossier::connect();
        try {
            $current=$this->get_current_version()+1;
            for ( $i = $current; $i < VERSION_IMPACC2;$i++) {
                $file=$g_impacc."/sql/upgrade".$i.".sql";
                if ( ! file_exists($file) ) {
                    throw new \Exception ("File $file does not exist");
                    
                }
                $cn->execute_script($file);
            }
            
        } catch (\Exception $e) {
            $cn->rollback();
            record_log($e->getTraceAsString());
            throw $e;
            
        }
    }
}

?>
