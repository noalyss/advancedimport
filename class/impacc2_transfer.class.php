<?php
namespace NoalyssImport;
/*
 *   This file is part of NOALYSS.
 *
 *   Noalyss is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Noalyss is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Noalyss; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2018) Author Dany De Bontridder <dany@alchimerys.be>


/**
 * @file
 * @brief 
 */
require_once DIR_IMPORT_ACCOUNT.'/class/import/impacc2_import_anc_csv.class.php';
require_once DIR_IMPORT_ACCOUNT.'/class/import/impacc2_file.class.php';

/**
 * @brief Class will manage the transfer to the accountancy and analytic, the 
 */
class Impacc_Transfer
{

    private $acc_id; //!< import_file.id
    private $a_anc_id; //!< array of anc_file import_file_anc.id
    private $transfer_log_id; //!< id of transfer

    function __construct($p_import_acc_id, $pa_import_anc_id="")
    {
        if (isNumber($p_import_acc_id)==0)
            throw new Exception(_("Paramètre invalide"));
        $this->acc_id=$p_import_acc_id;
        if ($pa_import_anc_id!="")
        {
            $this->a_anc_id=preg_split("/,/", $pa_import_anc_id);
            $this->a_anc_id=array_unique($this->a_anc_id);
        }
        else
        {
            $this->a_anc_id=array();
        }
        $this->transfer_log_id=0;
    }

    /**
     *@brief  Display the selected choice after checking their data
     */
    function confirm()
    {
        echo '<form method="post" onsubmit="waiting_box();return true;">';
        echo \HtmlInput::array_to_hidden(['sa', 'gDossier', 'plugin_code', 'ac', 'acc_file_h', 'anc_file_h'], $_REQUEST);
        echo \HtmlInput::hidden("sb", "transfer");
        echo \HtmlInput::submit("transfer", _("Confirme transfert"));
        echo '</form>        ';
        // Check the ACC file    
        $impacc_file=Impacc_File::new_object($this->acc_id);
        $this->correct_case();
        $impacc_file->check();

        // Display Acc File
        $impacc_file->result();

        //-- Analytic
        $nb_acc=count($this->a_anc_id);

        // Check and display all the selected files
        for ($i=0; $i<$nb_acc; $i++)
        {
            // Check the ANC FILES
            $idx=$this->a_anc_id[$i];
            $impacc_import_anc_csv=new Impacc_Import_Anc_Csv($idx);
            $impacc_import_anc_csv->check();

            // Display ANC Files
            $impacc_import_anc_csv->result();
        }
        echo '<form method="post">';
        echo \HtmlInput::array_to_hidden(['sa', 'gDossier', 'plugin_code', 'ac', 'acc_file_h', 'anc_file_h'], $_REQUEST);
        echo \HtmlInput::hidden("sb", "transfer");
        echo \HtmlInput::submit("transfer", _("Confirme transfert"));
        echo '</form>        ';
    }

    /**
     * Main function
     * Transfer to accountancy and analytic
     * @note this function is needless
     */
    function transfer_file()
    {
        $a_row=$this->transfer_acc();
    }
    /**
     * @brief correct case for card (or accounting)
     */
    function correct_case()
    {
        $cn=\Dossier::connect();
    }
    /**
     * @brief Transform each row from import_detail and insert it into the right ledger
     * and also the anc
     */
    function transfer_acc()
    {
         $impacc_file=Impacc_File::new_object($this->acc_id);
         $cn=\Dossier::connect();
         
         if ( $impacc_file->format=="CSV") 
         {

            $nb_row=$this->transfer_acc_csv();
         } elseif ( $impacc_file->format=="FEC") 
         {
           $nb_row=$this->transfer_acc_fec();
         } else {
            throw new Exception(_("TRANS01.Erreur transfert"));
         }
        $import_file=new \Impacc_Import_file_SQL($cn,$this->acc_id);
        $import_file->setp("i_date_import",date("d.m.Y H:i:s"));
        $import_file->update();
        printf(_("Opérations transférées %s"),$nb_row);
    }

    /// Create the right object 
    /// and throw and exception if the ledger type can not be found
    //\param Acc_Ledger $p_ledger 
    private function make_csv_class(\Acc_Ledger $p_ledger)
    {
        switch ($p_ledger->get_type())
        {
            case 'ACH':
                $obj=new Impacc_Csv_Purchase();
                break;
            case 'VEN':
                $obj=new Impacc_Csv_Sale();
                break;
            case 'ODS':
                $obj=new Impacc_Csv_Misc_Operation();
                break;
            case 'FIN':
                $obj=new Impacc_Csv_Bank();
                break;
            default :
                throw new Exception(_('type journal inconnu'));
        }
        return $obj;
    }
    
    /// Transfer data from CSV file
    private function transfer_acc_csv()
    {
        // For each row from import_detail where import_id = $this->acc_id
        // transform 
        $cn=\Dossier::connect();
        $cn->start();
        try
        {
            ///- Load       only the correct group (all the rows in the group must be valid)
            $sql="
                    with rejected as ( SELECT distinct id_code_group 
                                        FROM impacc2.import_detail a
                                        where 
                                        import_id=$1
                                        and (id_status != 0 or trim(COALESCE(id_message,'')) !='')
                                        ) 
                    select distinct id_code_group ,id_date_format_conv, import_id,ledger_code.jrn_def_id
                    from 
                        impacc2.import_detail 
                        join impacc2.ledger_code on (id_ledger_code=ledger_code)
                    where 
                        import_id=$1 
                      and id_code_group not in (select coalesce(id_code_group,'') from rejected)

                    order by id_date_format_conv asc
                    ";

            $a_group=$cn->get_array($sql, [$this->acc_id]);
            $nb_row=count($a_group);

            for ($i=0; $i<$nb_row; $i++)
            {
                // Create the dest. ledger
                $ledger=Impacc_Tool::ledger_factory($a_group[$i]['jrn_def_id']);

                // Create the right object for inserting
                $impacc_csv=$this->make_csv_class($ledger);

                // Compute variable for anc in $_POST for each row
                $a_anc_operation=$this->anc_to_array($a_group[$i]['id_code_group']);

                // Insert in the right ledger
                $a_insert=$impacc_csv->insert($a_group[$i], $ledger,$a_anc_operation);
            }
            $cn->commit();
            return $nb_row;
        }
        catch (Exception $exc)
        {
            echo $exc->getMessage();
            error_log($exc->getTraceAsString());
            $cn->rollback();
            throw $exc;
        }
        
    }
    
    /*!
    * @brief  Return an array for ANC compatible with Anc_Operation::save_form_plan, return an empty if there is no ANC
    *
    * @see Anc_Operation::save_form_plan
    * @param int $p_group_id
    */
    function anc_to_array($p_group_id)
    {
        if ($this->a_anc_id==NULL)
        {
            return [];
        }

        $cn=\Dossier::connect();
        
        // List of ANC CSV files
        $str_list=join(",", $this->a_anc_id);

        // retrieve all needed data , if there is no valid id_analytic_ref then return
        $a_anc_row=$cn->get_array("select distinct
            ida_amount,
            import_id,
            import_file_anc_id, 
            id_status,
            id_analytic_ref,
            id_code_group,
            ida_anc_account
            id_label,
            pa_id,
            id_amount_novat,
            import_detail_id,
            ida_anc_account,
            po_id
        from  
           impacc2.v_analytic_operation
        where
           import_id=$1 
           and id_code_group=$2
           and  import_file_anc_id in ($str_list)
           order by pa_id,id_analytic_ref", [
            $this->acc_id,
            $p_group_id
        ]);
        $nb_anc_row=count($a_anc_row);
        if ($nb_anc_row==0)
        {
            return [];
        }
        // result 
        $a_ancrow_result=[];
        
        // 1. p_item nb of rows in an operation
        $a_operation=$cn->get_array("select id,id_date,id_acc
                                from 
                                    impacc2.import_detail as id1
                                where  
                                    id_code_group=$1
                                    and id1.import_id=$2", [$p_group_id, $this->acc_id]);

        // 2. Plan 
        $a_pa_id=explode(",", $cn->make_list("select pa_id from plan_analytique order by pa_id "));
        $a_ancrow_result['pa_id']=$a_pa_id;

        //for each row , we have an hplan with all the value for each plan
        $a_hplan=[];
        $idx=0;

        /**
         * For each different row , we need to compute , hplan and val
         */

        $nb_operation = count($a_operation);

        // $idx_operation is used to see which row has analytic info
        $idx_operation=-1;

        /// @var $g_parameter \Noalyss_Parameter_Folder($p_cn)
        global $g_parameter;

        ///@var $fiche \Fiche will be used to check that Analytic is appliable or not
        /// and then increase or not $idx_operation
        $fiche=new \Fiche($cn);

        for ($e=0; $e< $nb_operation; $e++)
        {
            // compute properly the operation row number ($idx_operation)
            // for Anc_Operation(p_item : $idx_operation)
            // $a_operation[$e]['id_acc'] is either a CARD or an Accounting
            if ($cn->get_value('select count(*) from tmp_pcmn where pcm_val=$1',[$a_operation[$e]['id_acc']]) == 1)
            {
                if ($g_parameter->match_analytic($a_operation[$e]['id_acc']) == true)                 $idx_operation++;
            } else {

                $account=$cn->get_value("select fd2.ad_value 
                    from fiche_detail fd1 
                        join fiche_detail fd2 on (fd1.f_id=fd2.f_id)
                    where
                         fd1.ad_id=$1 and fd2.ad_id=$2 and fd1.ad_value=$3",[ATTR_DEF_QUICKCODE,ATTR_DEF_ACCOUNT,$a_operation[$e]['id_acc']]);

                if ($account != "" && $g_parameter->match_analytic( $account)==true ) {
                    $idx_operation++;
                }
            }
           $a_row_operation=  $cn->get_array("select distinct
                                ida_amount,
                                import_id,
                                import_file_anc_id, 
                                id_status,
                                id_analytic_ref,
                                id_code_group,
                                ida_anc_account
                                id_label,
                                pa_id,
                                id_amount_novat,
                                import_detail_id,
                                ida_anc_account,
                                po_id,
                                id_acc
                            from  
                               impacc2.v_analytic_operation
                            where
                               import_id=$1 
                               and id_code_group=$2
                               and  import_file_anc_id in ($str_list)
                               and import_detail_id=$3
                               order by pa_id,id_analytic_ref", [
                                    $this->acc_id,
                                    $p_group_id,
                                    $a_operation[$e]['id']
                                ]);

            if (empty($a_row_operation)) continue;
            // @var id_acc is either an accountign or a card
         //   $id_acc = $a_row_operation['id_acc'];

            // in $a_data we have all the occurences , so first , we fill with the pa_id
            $nb_data=count($a_row_operation);
            $n_plan=count($a_pa_id);
            $a_plan=[];
            if (DEBUGIMPORTADV > 0)
            {
                echo h1("\$a_row_operation ");
                var_dump($a_row_operation);
            }
            // Number of subdivision for one rows
            $max_row=0;
            for ($k=0; $k<$n_plan; $k++)
            {
                $tmp_max=$cn->get_value("select count(*) 
                    from 
                        impacc2.v_analytic_operation
                        join public.poste_analytique on (po_name=ida_anc_account)
                     where
                        import_id=$1 
                        and import_file_anc_id in ($str_list)
                        and id_code_group=$2
                        and import_detail_id=$3
                        and poste_analytique.pa_id=$4
                        ",
                        [
                    $this->acc_id,
                    $p_group_id,
                    $a_row_operation[0]['import_detail_id'],
                    $a_pa_id[$k]
                ]);
                if ($tmp_max>$max_row)
                {
                    $max_row=$tmp_max;
                }
            }

            // Fill with 0 a $max_row * n_plan matrix
            for ($j=0; $j<$max_row*$n_plan; $j++)
            {
                $a_plan[$j]=-1;
            }

            $pos=0;
            if (DEBUGIMPORTADV > 0)
            {
                echo h1("a_plan");
                var_dump($a_plan);
            }
            /**
             * Store data in different structure
             */
            $a_operation_axis=array();
            for ($j=0; $j<$nb_data; $j++)
            {
                $idx_pa_id=$a_row_operation[$j]['pa_id'];
                if (DEBUGIMPORTADV > 0 ){
                    echo h2("idx_pa_id ".$idx_pa_id);
                    var_dump($a_operation_axis);
                }
                $a_operation_axis[$idx_pa_id][]= $a_row_operation[$j]['po_id'];
            }

            for ($j = 0;$j < $n_plan;$j++)
            {
                $idx_pa_id=$a_pa_id[$j];
                if ( isset($a_operation_axis[$idx_pa_id]) && ! empty ($a_operation_axis[$idx_pa_id]))
                {
                    $nb_operation_axis=count($a_operation_axis[$idx_pa_id]);
                    for ($k=0;$k < $nb_operation_axis;$k++)
                    {
                        $pos=$j+$k*$n_plan;
                        $a_plan[$pos]=$a_operation_axis[$idx_pa_id][$k];
                    }
                }
            }
            
            if (DEBUGIMPORTADV > 0 ) {
                echo h1("a_operation_axis");
                var_dump($a_operation_axis);
            }
            // The operation idx is a operation with anc, it is not the compteur
            

            $a_ancrow_result["hplan"][$idx_operation]=$a_plan;

            $a_ancrow_result["op"][$idx_operation]=$idx_operation;


            //-- subdivision for each row , the amount of the item is splitted into 2 parts = 2 subdivisions , that 
            //   mean there are twice an amount for the same analytic axis. Then it exists 2 analytics axis for a row
            $a_unique_used_analytic_plan=array_unique(array_column($a_anc_row, "pa_id"));
            if (count($a_unique_used_analytic_plan)>0)
            {
                $tmp_unique=[];
                $idx=0;
                foreach ($a_unique_used_analytic_plan as $key=> $value)
                {
                    $tmp_unique[$idx]=$value;
                    $idx++;
                }
                $a_unique_used_analytic_plan=$tmp_unique;
            }

            $idx=0;
            $a_done=[];
            if (DEBUGIMPORTADV > 0)
            {
                echo h1("a_unique_used_analytic_plan");
                var_dump($a_unique_used_analytic_plan);
            }
            
            
            // For each plan , we add rows
            //
            for ($x=0; $x<count($a_unique_used_analytic_plan); $x++)
            {
                for ($j=0; $j<$nb_data; $j++)
                {
                    // If the axis is not yet and we found a valid number, then save it and increase idx (=subdivision)
                    if (in_array($a_unique_used_analytic_plan[$x], $a_done)==FALSE
                        &&isNumber($a_row_operation[$j]['ida_amount'])==1
                        )
                    {
                        $a_ancrow_result['val'][$idx_operation][$idx]=$a_row_operation[$j]['ida_amount'];
                        $idx++;
                    }
                    //$a_done[]=$a_unique_used_analytic_plan[$x];
                }
            }

            if (DEBUGIMPORTADV > 0)
            {
                echo h1('$a_ancrow_result');
                var_dump($a_ancrow_result);
            }

        }
        if (DEBUGIMPORTADV > 0)
        {
            echo h1('final $a_ancrow_result');
            var_dump($a_ancrow_result);
        }

        
        // return the computed array for anc
        return $a_ancrow_result;
    }
    /// Transfer data from FEC
    private function transfer_acc_fec()
    {
      // For each row from import_detail where import_id = $this->acc_id
        // transform 
        $cn=\Dossier::connect();
        
        try
        {
            $cn->start();
            ///- Load       only the correct group (all the rows in the group must be valid)
            $sql="
                    with rejected as ( SELECT distinct id_code_group 
                                        FROM impacc2.import_detail a
                                        where 
                                        import_id=$1
                                        and (id_status != 0 or trim(COALESCE(id_message,'')) !='')
                                        ) 
                    select distinct id_code_group ,id_date_format_conv, import_id,ledger_code.jrn_def_id
                    from 
                        impacc2.import_detail 
                        join impacc2.ledger_code on (id_ledger_code=ledger_code)
                    where 
                        import_id=$1 
                        and id_code_group not in (select coalesce(id_code_group,'') from rejected)
                    order by id_date_format_conv asc
                    ";

            $a_group=$cn->get_array($sql, [$this->acc_id]);
            $nb_row=count($a_group);
            
            $impacc_fec=new Operation_Fec();
            
            Impacc_FEC::create_accounting($this->acc_id);
            
            for ($i=0; $i<$nb_row; $i++)
            {
                $ledger=new \Acc_Ledger($cn,$a_group[$i]['jrn_def_id']);
                // Insert in the right ledger
                $a_insert=$impacc_fec->insert($a_group[$i], $ledger);
            }
            $cn->commit();
            return $nb_row;
        }
        catch (Exception $exc)
        {
            echo $exc->getMessage();
            error_log($exc->getTraceAsString());
            $cn->rollback();
            throw $exc;
        }
    }
}
