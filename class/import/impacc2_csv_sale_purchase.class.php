<?php
namespace NoalyssImport;
/*
 * Copyright (C) 2016 Dany De Bontridder <dany@alchimerys.be>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
  ///!@file
  ///Insert into the the table impacc2.import_detail + status
 *
 */
require_once DIR_IMPORT_ACCOUNT.'/class/import/impacc2_csv.class.php';
require_once DIR_IMPORT_ACCOUNT.'/class/import/impacc2_verify.class.php';
require_once DIR_IMPORT_ACCOUNT.'/class/import/impacc2_operation.class.php';


///Manage one row of operation of Sale / Purchase for importing them
abstract class Impacc_Csv_Sale_Purchase extends Operation
{

    ///Works by id_group_code : all the rows with the same id_code_group belong
    ///to the same operation
    ///The operation will be written in the right ledger 
    ///Only operation with a id_status = 0 can be transferred , -1 is in error and
    ///1 means that it has already  been transferred
    function transform($ledger_id, $import_id)
    {
        throw new Exception("Not Yet Implemented");
    }

    //-------------------------------------------------------------------
    /// Check if Data are valid for one row
    //!@param $row is an Impacc_Import_detail_SQL object
    //!@param $p_format_date check for date_limit and date_payment
    //!@param $p_thousand separator for thousands
    //!@param $p_decimal separator for decimal
    //-------------------------------------------------------------------
    static function check(\Impacc_Import_detail_SQL $row, $p_format_date,
            $p_thousand, $p_decimal)
    {
        $cn=\Dossier::connect();
        $and=($row->id_message=="")?"":",";
        //-----------------
        ///- Check VAT CODE
        //-----------------
        if (Impacc_Verify::check_tva($row->tva_code)==false)
        {
            $row->id_message.=$and.sprintf(_("TVA invalide [%s]"),$row->tva_code);
            $and=",";
        }
        //-----------------
        ///- Check Date payment
        //-----------------
        if ($row->id_date_payment!="")
        {
            $date=Impacc_Verify::check_date($row->id_date_payment,$p_format_date);
            if ($date==false)
            {
                $row->id_message.=$and.sprintf(_("Date incorrecte [%s]"),$row->id_date_payment);
                $and=",";
            } else {
                $row->id_date_payment_conv=$date;
            }
        }
        //-----------------
        ///- Check Date limit
        //-----------------
        if ($row->id_date_limit!="")
        {
            $date=Impacc_Verify::check_date($row->id_date_limit,$p_format_date);
            if ($date==false)
            {
                $row->id_message.=$and.sprintf(_("Date incorrecte [%s]"),$row->id_date_limit);
                $and=",";
            } else {
                $row->id_date_limit_conv=$date;
            }
        }

        //-----------------
        ///- Check Amount VAT
        //-----------------
        $row->id_amount_vat_conv=Impacc_Tool::convert_amount($row->id_amount_vat,
                        $p_thousand, $p_decimal);
        if (isNumber($row->id_amount_vat_conv)==0)
        {
            $and=($row->id_message=="")?"":",";
            $row->id_message.=$and.sprintf(_("Montant incorrect [%s]"),$row->id_amount_vat_conv);
        }
        //-----------------
        ///- Check Quantity
        //-----------------
        $row->id_quant_conv=Impacc_Tool::convert_amount($row->id_quant,
                        $p_thousand, $p_decimal);
        if (isNumber($row->id_quant_conv)==0)
        {
            $row->id_message.=$and.sprintf(_("Montant incorrect [%s]"),$row->id_quant_conv);
            $and=",";
        }

    }
    
    function check_nb_column()
    {
        throw new Exception("Not Yet Implemented");
    }

    /**
     * @brief insert file into the table import_detail
     */
    function record(Impacc_Import $p_csv, Impacc_File $p_file)
    {
        global $aseparator;
        // Open the CSV file
        $hFile=fopen($p_file->import_file->i_tmpname, "r");
        $error=0;
        $cn=\Dossier::connect();
        $delimiter=\NoalyssImport\Impacc_Tool::get_char_delimiter($p_csv->detail->s_delimiter);
        
        $s_surround=\NoalyssImport\Impacc_Tool::get_char_surround($p_csv->detail->s_surround);
        $in_char=\NoalyssImport\Impacc_Tool::get_encoding($p_csv->detail->s_encoding);

        //---- For each row ---
        while ($row=fgetcsv($hFile, 0, $delimiter, $s_surround))
        {
            $nb_row=count($row);
            $insert=new \Impacc_Import_detail_SQL($cn);
            $insert->import_id=$p_file->import_file->id;
            if ($nb_row<9)
            {
                $insert->id_status=-1;
                $insert->id_message=join($delimiter,$row);
            }
            else
            {
                $insert->id_date=$row[0];
                $insert->id_ledger_code=$row[1];
                $insert->id_code_group=$row[2];
                $insert->id_pj=$row[3];
                $insert->id_acc=$row[4];
                $insert->id_label=$row[5];
                $insert->id_acc_second=$row[6];
                $insert->id_quant=$row[7];
                $insert->id_amount_novat=$row[8];
                $insert->tva_code=$row[9];
                $insert->id_amount_vat=$row[10];
                $date_limit=(isset($row[11]))?$row[11]:"";
                $insert->id_date_limit=$date_limit;
                $date_payment=(isset($row[12]))?$row[12]:"";
                $insert->id_date_payment=$date_payment;
                // Analytic ref.
                $insert->id_analytic_ref=(isset($row[13]))?$row[13]:"";
                $insert->id_currency_code=(isset($row[14]))?$row[14]:"";
                $insert->id_currency_amount=(isset($row[15]))?$row[15]:"";
                $insert->id_cardlabel=(isset($row[16]))?$row[16]:"";
                $insert->id_nb_row=0;
            }
            // insert row into table with status
            $insert->insert();
        }
    }

    /**
     * Retrieve the currency value and code, 
     * @param string $p_code , code iso of the currency
     * @param int $p_code_id id of the currency
     * @param float $p_rate rate of the currency
     * @see Impacc_CSV_Purchase::adapt
     * @see Impacc_CSV_Sale::adapt
     * 
     */
    function set_currency($p_code,& $p_code_id, & $p_rate)
    {
        $cn=\Dossier::connect();
        $use_currency=FALSE;
        if (file_exists( NOALYSS_INCLUDE."/class/acc_currency.class.php"))
        {
            require_once NOALYSS_INCLUDE."/class/acc_currency.class.php";
            $use_currency=TRUE;

        }

        if ($p_code != NULL && $use_currency == TRUE)
        {
            $currency=new \Acc_Currency($cn);
            $currency->get_by_iso($p_code);
            $p_rate=($p_rate>0)?$p_rate:$currency->get_rate();
            $p_code_id=$currency->get_id();
        }
        else
        {
            $p_rate=1;
            $p_code_id=0;
        }
    }

}
