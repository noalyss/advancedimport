<?php
namespace NoalyssImport;
/*
 *   This file is part of NOALYSS.
 *
 *   Noalyss is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Noalyss is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Noalyss; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2018) Author Dany De Bontridder <dany@alchimerys.be>

/**
 * @file
 * @brief Used by all Import CSV Operation for analytic, contains the setting (delimiter,thousand ...) 
 */
require_once DIR_IMPORT_ACCOUNT.'/database/impacc2_import_file_anc_sql.class.php';
require_once DIR_IMPORT_ACCOUNT.'/database/impacc2_import_detail_anc_sql.class.php';

///Used by all Import CSV Operation for analytic, contains the setting (delimiter,thousand ...) 
class Impacc_Import_Anc_Csv
{

    var $detail; //!< Object Impacc_Import_file_anc_SQL
    var $errcode; //!< Array of error code will be recorded in import_detail.id_message
    /**
     * 
     * @param INT $p_id pk of the table import_file_anc
     */
    function __construct($p_id=-1)
    {
        $cn=\Dossier::connect();
        $this->detail=new \Impacc_Import_file_anc_SQL($cn, $p_id);
        if ($p_id==-1)
        {
            $this->detail->ifa_delimiter="2";
            $this->detail->ifa_surround='"';
            $this->detail->ifa_decimal='1';
            $this->detail->ifa_thousand='0';
        }
        $this->errcode=array(
            "CK_INVALID_AMOUNT"=>_("Montant invalide"),
            "CK_INVALID_ACCOUNTING"=>_("Poste comptable ou Fiche non existante"),
        );
    }

    /**
     * Return the row from import_file_anc
     * @return Impacc_Import_file_anc_SQL
     */
    function get_detail()
    {
        return $this->detail;
    }

    function get_errcode()
    {
        return $this->errcode;
    }
    /**
     * Set Impacc_Import_file_anc_SQL
     * @param Impacc_Import_file_anc_SQL $detail
     */
    function set_detail($detail)
    {
        $this->detail=$detail;
    }

    function set_errcode($errcode)
    {
        $this->errcode=$errcode;
    }

    /// Display a form to upload a CSV file with operation
    function input_format()
    {
        global $cn, $adecimal, $athousand, $aseparator, $aformat_date,$asurround;
        $in_delimiter=new \ISelect('in_delimiter');
        $in_delimiter->value=$aseparator;
        $in_delimiter->selected=$this->detail->ifa_delimiter;
        $in_delimiter->size=1;

        $in_surround=new \ISelect('in_surround');
        $in_surround->value=$asurround;
        $in_surround->size=1;


        $in_decimal=new \ISelect('in_decimal');
        $in_decimal->value=$adecimal;
        $in_decimal->selected=$this->detail->ifa_decimal;
        $in_decimal->size=1;

        $in_thousand=new \ISelect('in_thousand');
        $in_thousand->selected=$this->detail->ifa_thousand;
        $in_thousand->value=$athousand;
        $in_thousand->size=1;

        require DIR_IMPORT_ACCOUNT.'/template/upload_operation_anc.php';
    }

    /**
     * Check that parameters are valid
     * @throws Exception
     */
    function check_setting()
    {
        // Check if valid
        // 1 sep for thousand and decimal MUST be different
        if ($this->detail->ifa_thousand==$this->detail->ifa_decimal)
        {
            throw new Exception(_("Séparateur de décimal et milliers doivent être différents"));
        }
    }

    /**
     * Check data in table
     */
    function check()
    {
       $cn=\Dossier::connect();
       // Reset all code
       $cn->exec_sql("update impacc2.import_detail_anc set ida_message=NULL where import_file_anc_id=$1",
               [$this->detail->id]);
       
       // Missing analytic account
       $cn->exec_sql("update impacc2.import_detail_anc set ida_message=coalesce(ida_message,'')||$1 where import_file_anc_id=$2"
               . "                and ida_anc_account not in (select po_name from public.poste_analytique) "
               ,['CK_INVALID_ACCOUNTING,',$this->detail->id]);
       
       // Invalid number
       // if decimal is comma then replace by point
       if ( $this->detail->ifa_decimal == 1 ) 
       {
           $cn->exec_sql ("update impacc2.import_detail_anc set ida_amount=replace(ida_amount,',','.') where import_file_anc_id=$1  ",
                   [$this->detail->id]);
       }
       // if thousand is space then remove
       if ( $this->detail->ifa_thousand == 0 ) 
       {
           $cn->exec_sql ("update impacc2.import_detail_anc set ida_amount=replace(ida_amount,' ','') where import_file_anc_id=$1  ",
                   [$this->detail->id]);
       }
       // if thousand is comma than replace by point
       if ( $this->detail->ifa_thousand == 1 ) 
       {
           $cn->exec_sql ("update impacc2.import_detail_anc set ida_amount=replace(ida_amount,',','') where import_file_anc_id=$1  ",
                   [$this->detail->id]);
       }
       // if thousand is comma than replace by point
       if ( $this->detail->ifa_thousand == 2 ) 
       {
           $cn->exec_sql ("update impacc2.import_detail_anc set ida_amount=replace(ida_amount,'.','') where import_file_anc_id=$1  ",
                   [$this->detail->id]);
       }
       
       $cn->exec_sql("update impacc2.import_detail_anc set ida_message=coalesce(ida_message,'')||$1 where import_file_anc_id=$2 
                and isnumeric (ida_amount) = false"
               ,['CK_INVALID_AMOUNT,',$this->detail->id]);
       
       
    }

    /**
     *  Save setting
     */
    function save_setting()
    {
        $this->detail->save();
    }

    /// Thank the import_file.id we find the corresponding record from import_csv
    /// and we load id
    //! \param $p_import_id is impacc2.import_file.id
    function upload_file()
    {
        if (trim($_FILES['file_operation']['name']??"")=='')
        {
            alert(_('Pas de fichier donné'));
            return -1;
        }
        $filename=$_ENV['TMP']."/".tmpfile();
        if (!move_uploaded_file($_FILES["file_operation"]["tmp_name"], $filename))
        {
            throw new Exception(_("Fichier non sauvé"), 1);
        }
        return $filename;
    }

    /**
     * After uploading file, record element into table import_detail_anc
     */
    function record($p_filename)
    {
        global $aseparator, $athousand, $adecimal;
        $cn=\Dossier::connect();
        //1. Open uploaded file 
        $hFile=fopen($p_filename, "r");

        //2. Read file 
        $delimiter=\NoalyssImport\Impacc_Tool::get_char_delimiter($this->detail->ifa_delimiter);
        $athousand=\NoalyssImport\Impacc_Tool::get_char_thousand($this->detail->ifa_thousand);
        $decimal=\NoalyssImport\Impacc_Tool::get_char_decimal($this->detail->ifa_decimal);
        $surround=\NoalyssImport\Impacc_Tool::get_char_surround($this->detail->ifa_surround);

        //3. Insert into table
        while ($row=fgetcsv($hFile, 0, $delimiter, $surround))
        {
            if (count($row)<3)
            {
                continue;
            }
            $record=new \Impacc_Import_detail_anc_SQL($cn);
            $record->id_analytic_ref=$row[0];
            $record->ida_amount=str_replace(',', '.', $row[1]);
            $record->ida_anc_account=$row[2];
            $record->import_file_anc_id=$this->detail->id;
            $record->insert();
        }
    }

    function result()
    {
        $http=new \HttpInput();
        $cn=\Dossier::connect();
        $plugin_code=$http->request("plugin_code");
        $import_detail=new \Impacc_Import_detail_anc_SQL($cn);
        $manage_table=new \Manage_Table_SQL($import_detail);
        $manage_table->add_json_param("plugin_code", $plugin_code);
        $manage_table->set_update_row(FALSE);
        $manage_table->set_append_row(FALSE);
        printf("<h2>"._("Importation Analytique n° %s")."</h2>", $this->detail->id);
        $manage_table->set_order(["id_analytic_ref","ida_amount","ida_anc_account","ida_message"]);
        $manage_table->set_col_label("id_analytic_ref",_("Ref analytique"));
        $manage_table->set_col_label("ida_amount",_("Montant"));
        $manage_table->set_col_label("ida_anc_account",_("Poste analytique"));
        $manage_table->set_col_label("ida_message",_("Résultat"));
        $manage_table->display_table(" where import_file_anc_id = $1 order by id",array($this->detail->id));
        $manage_table->create_js_script();
    }
    
    function select_file($p_selected)
    {
        $cn=\Dossier::connect();
        $sql_selected="";
        
        if ( $p_selected != "")
        {
            $a_selected=preg_split("/,/",$p_selected);
            $n=count($a_selected);
            $sql_list="";
            $sep="";
            for ($e=0;$e<$n;$e++) {
                if ( isNumber($a_selected[$e]) == 1) {
                    $sql_list.=$sep.$a_selected[$e];
                    $sep=",";
                }
            }
            if ($sql_list != ""){
                $sql_selected=" where id not in ( $sql_list )";
            }
        }
        $array=$cn->get_array(" 
            select id,
                    ifa_filename,
                    to_char(ifa_date,'DD.MM.YY HH24:MI') as str_date,
                    ifa_date 
                from 
                impacc2.import_file_anc 
                $sql_selected
                order by id desc");
        require_once DIR_IMPORT_ACCOUNT."/template/select-anc-file.php";
    }

}
