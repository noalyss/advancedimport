<?php
namespace NoalyssImport;
/*
 *   This file is part of NOALYSS.
 *
 *   Noalyss is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Noalyss is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Noalyss; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2018) Author Dany De Bontridder <dany@alchimerys.be>

require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_file.class.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_import_anc_csv.class.php";

/**
 * @file
 * @brief Import files , put together CSV Anc and accountancy
 */

///Import files , put together CSV Anc and accountancy
class Impacc_Import 
{
    var $detail; //!< Object Impacc_Import_csv_SQL
    var $errcode; //!< Array of error code will be recorded in import_detail.id_message
    
    function __construct()
    {
         $this->errcode=array(
            "CK_FORMAT_DATE"=>_("Format de date incorrect"),
            "CK_PERIODE_CLOSED"=>_("Période non trouvée"),
            "CK_INVALID_PERIODE"=>_("Période non trouvée"),
            "CK_INVALID_AMOUNT"=>_("Montant invalide"),
            "CK_INVALID_ACCOUNTING"=>_("Poste comptable ou Fiche non existante"),
            "CK_TVA_INVALID"=>_("Code TVA Invalide"),
            "CK_CARD_LEDGER"=>_("Fiche non disponible pour journal"),
            "CK_BALANCE"=>_("Balance incorrecte"),
            "CK_ERROR_DEBIT"=>_("Erreur D/C"),
            "CK_CURRENCY_CODE"=>_("Code devise invalide"),
            "CK_CURRENCY_AMOUNT"=>_("Montant devise invalide")
        );

    }
     /// Display the parameters and the file
    /// to upload
    static function input_file_acc()
    {
        require_once DIR_IMPORT_ACCOUNT."/template/input_file_acc.php";
    }
    /// Display the parameters and the file
    /// to upload
    static function input_file_anc()
    {
        require_once DIR_IMPORT_ACCOUNT."/template/input_file_anc.php";
    }

    function set_import($p_file_id)
    {
        $this->detail->import_id=$p_file_id;
    }

    function get_import_id() {
        if (empty($this->detail) ) {
            throw new \Exception ("IP73.Detail not loaded".__FUNCTION__);
        }
        return $this->detail->import_id;
    }

    public function check_setting()
    {
        
    }


    public function set_setting()
    {
        
    }
        ///Save the Impacc_Import_csv_SQL object into db
    function save_setting()
    {
        $this->detail->save();
    }
    /// Integrity , a code group cannot have 2 date 
    function check_integrity_code_group()
    {
        $cn=\Dossier::connect();
        $sql="
            
             "   ;
        $sql->exec_sql($sql,$this->detail->import_id);
    }
    function check_form()
    {

    }


}