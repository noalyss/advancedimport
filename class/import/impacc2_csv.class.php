<?php
namespace NoalyssImport;

/*
 * Copyright (C) 2016 Dany De Bontridder <dany@alchimerys.be>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


require_once DIR_IMPORT_ACCOUNT.'/class/import/impacc2_csv_bank.class.php';
require_once DIR_IMPORT_ACCOUNT.'/class/import/impacc2_csv_sale.class.php';
require_once DIR_IMPORT_ACCOUNT.'/class/import/impacc2_csv_purchase.class.php';
require_once DIR_IMPORT_ACCOUNT.'/class/import/impacc2_csv_misc_operation.class.php';
require_once DIR_IMPORT_ACCOUNT.'/class/impacc2_tool.class.php';
require_once DIR_IMPORT_ACCOUNT."/database/impacc2_import_detail_sql.class.php";
require_once NOALYSS_INCLUDE."/lib/http_input.class.php";
require_once NOALYSS_INCLUDE."/lib/manage_table_sql.class.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_import.class.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/DetailCsv_MTable.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/DetailCsv_Financial_MTable.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/DetailCsv_Misc_MTable.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/DetailCsv_Purchase_MTable.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/DetailCsv_Sale_MTable.php";

///Used by all Import CSV Operation , contains the setting (delimiter,thousand ...) 
class Impacc_CSV extends Impacc_Import
{


    function __construct()
    {
        parent::__construct();
        $cn=\Dossier::connect();
        $this->detail=new \Impacc_Import_csv_SQL($cn);
        $this->detail->s_delimiter="2";
        $this->detail->s_surround=0;
        $this->detail->ledger_type='ODS';
        $this->detail->s_encoding="utf-8";
        $this->detail->s_decimal='1';
        $this->detail->s_thousand='0';
        $this->detail->s_date_format=1;
       
    }

    /// Display a form to upload a CSV file with operation
    function input_format()
    {
        global $cn, $adecimal, $athousand, $aseparator, $aformat_date,$asurround;
        $in_delimiter=new \ISelect('in_delimiter');
        $in_delimiter->value=$aseparator;
        $in_delimiter->selected=$this->detail->s_delimiter;
        $in_delimiter->size=1;

        $in_surround=new \ISelect('in_surround', $this->detail->s_surround);
        $in_surround->value=$asurround;

        $in_ledger=new \ISelect('in_ledger');
        $in_ledger->value=[
            ["value"=>'ACH', "label"=>_('Achat')],
            ["value"=>'VEN', "label"=>_('Vente')],
            ["value"=>'FIN', "label"=>_('Financier')],
            ["value"=>'ODS', "label"=>_('Opérations Diverses')]
        ];

        $in_encoding=new \ISelect('in_encoding');
        $in_encoding->value=array(
            array('value'=>"utf-8", 'label'=>_('Unicode')),
            array('value'=>"windows", 'label'=>_('Windows-1252')),
            array('value'=>"latin1", 'label'=>_('Latin'))
        );
        $in_encoding->selected=$this->detail->s_encoding;

        $in_decimal=new \ISelect('in_decimal');
        $in_decimal->value=$adecimal;
        $in_decimal->selected=$this->detail->s_decimal;
        $in_decimal->size=1;

        $in_thousand=new \ISelect('in_thousand');
        $in_thousand->selected=$this->detail->s_thousand;
        $in_thousand->value=$athousand;
        $in_thousand->size=1;

        $in_date_format=new \ISelect("in_date_format");
        $in_date_format->value=$aformat_date;
        $in_date_format->selected=$this->detail->s_date_format;

        require DIR_IMPORT_ACCOUNT.'/template/upload_operation_csv.php';
    }



    //---------------------------------------------------------------------
    ///Get value from post , fill up the Impacc_Import_csv_SQL object
    //---------------------------------------------------------------------

    function set_setting()
    {
        $http=new \HttpInput();
        $this->detail->s_delimiter=$http->post("in_delimiter");
        $this->detail->s_surround=$http->post("in_surround");
        $this->detail->ledger_type=$http->post("in_ledger");
        $this->detail->s_encoding=$http->post("in_encoding");
        $this->detail->s_decimal=$http->post("in_decimal");
        $this->detail->s_thousand=$http->post("in_thousand");
        $this->detail->s_date_format=$http->post("in_date_format", "number");
    }

    function check_setting()
    {
        // Check if valid
        // 1 sep for thousand and decimal MUST be different
        if (\NoalyssImport\Impacc_Tool::get_char_thousand($this->detail->s_thousand)
                ==
            \NoalyssImport\Impacc_Tool::get_char_decimal($this->detail->s_decimal)
            )
        {
            throw new Exception(_("Séparateur de décimal et milliers doivent être différent"));
        }
        //2 encoding and delimiter can not be empty
        //3 ledger must be writable for user
        //4 Check Date format
    }
    /// Thank the import_file.id we find the corresponding record from import_csv
    /// and we load id
    //! \param $p_import_id is impacc2.import_file.id
    function load_import($p_import_id)
    {
        try
        {
            $cn=\Dossier::connect();
            $id=$cn->get_value('select id from impacc2.import_csv where import_id=$1', array($p_import_id));
            $this->detail=new \Impacc_Import_csv_SQL($cn, $id);
            $this->detail->load();
        }
        catch (Exception $e)
        {
            echo $e->getMessage();
        }
    }
    /// @brief Check that upload file is correct
    /// Before transfer , we have to check also : 
    ///   # card belong to ledger
    ///   # Periode not closed
    function check(Impacc_File $p_file)
    {
        global $aformat_date;

        $this->load_import($p_file->impid);
        $ledger_type=$this->detail->ledger_type;
        // connect to DB
        $cn=\Dossier::connect();
        
        // Reset status
        $cn->exec_sql("update impacc2.import_detail set id_status =0 ,id_message=null where import_id=$1",
               [$p_file->impid]);
        
        
        // load all rows where status != -1
        $t1=new \Impacc_Import_detail_SQL($cn);
        $array=$t1->collect_objects(" where import_id = $1 and coalesce(id_status,0) <> -1 ", array($p_file->impid));
        // for each row check 
        $nb_array=count($array);
        $date_format=$aformat_date[$this->detail->s_date_format-1]['format'];
        $date_format_sql=$aformat_date[$this->detail->s_date_format-1]['label'];

        for ($i=0; $i<$nb_array; $i++)
        {
            $and=($array[$i]->id_message=="")?"":",";
            $array[$i]->id_status=0;
            if (trim($array[$i]->id_code_group??"")=="")
            {
                $array[$i]->id_status=-1;
                $array[$i]->id_message.=$and._(_("Code groupe vide"));
                $and=",";
            }
            //----------------------------------------------
            // Check ledger code
            //----------------------------------------------
            $ledger_actual=$cn->get_value("select jrn_def_id from impacc2.ledger_code where ledger_code=$1",[$array[$i]->id_ledger_code]);
            if ( $ledger_actual == "" || isNumber($ledger_actual )==0)
            {
                $array[$i]->id_message.=$and.sprintf(_("Aucun journal trouvé [%s]"),$array[$i]->id_ledger_code);
                $and=",";
                $ledger_actual=-1;
            }
            
            //------------------------------------
            //Check date format
            //------------------------------------
            $test=\DateTime::createFromFormat($date_format??"", $array[$i]->id_date??"");
            if ($test==false)
            {
                $array[$i]->id_status=-1;
                $array[$i]->id_message.=$and.sprintf(_("Date malformée [%s]"),$array[$i]->id_date);
                $and=",";
            }
            else
            {
                $array[$i]->id_date_conv=$test->format('d.m.Y');
                $array[$i]->id_date_format_conv=$test->format('Ymd');
                // Check if date exist and in a open periode
                $sql=sprintf("select p_id from parm_periode where p_start <= to_date($1,'%s') and p_end >= to_date($1,'%s') ",
                        $date_format_sql, $date_format_sql);
                $periode_id=$cn->get_value($sql, array($array[$i]->id_date));
                if ($cn->size()==0)
                {
                    $array[$i]->id_message.=$and.sprintf(_("Période inexistante pour [%s]"),$array[$i]->id_date_conv);
                    
                    $and=",";
                }
                else
                {
                  // Check that this periode is open for this ledger
                  $per=new \Periode($cn, $periode_id);
                  $per->jrn_def_id=$ledger_actual;
                    if ( $ledger_actual > 0 && $per->is_open()==0)
                  {
                    $array[$i]->id_message.=$and.sprintf(_("Période fermée pour [%s]"),$array[$i]->id_date);
                    $and=",";
                  }
                }
            }
            //----------------------------------------------------------------
            // Check that first id_acc does exist , for ODS it could be an
            // accounting, the card must be accessible for the ledger
            //----------------------------------------------------------------
            $card=Impacc_Verify::check_card($array[$i]->id_acc);
            if ($ledger_type=='ODS'&&$card==false)
            {
                // For ODS it could be an accounting
                $poste=new \Acc_Account_Ledger($cn, $array[$i]->id_acc);
                if ($poste->do_exist()==0)
                {
                    $array[$i]->id_message.=$and.sprintf(_("Poste comptable inconnu [%s]"),$array[$i]->id_acc);
                    $and=",";
                }
            }
            if ($ledger_type!='ODS'&&$card==false)
            {
                 $array[$i]->id_message.=$and.sprintf(_("Fiche inconnue [%s]"),$array[$i]->id_acc);
                $and=",";
            }
            // If card is valid check if belong to ledger
            if ($ledger_actual > 0  && $card instanceof Fiche)
            {
                if ($card->belong_ledger($ledger_actual) != 1 )
                {
                    $array[$i]->id_message.=$and.sprintf(_("Fiche [%s] inutilisable pour ce journal "),$array[$i]->id_acc);
                    $and=",";
                }
            }
            //---------------------------------------------------------------
            // Check amount
            // --------------------------------------------------------------

            $array[$i]->id_amount_novat_conv=Impacc_Tool::convert_amount($array[$i]->id_amount_novat,
                            $this->detail->s_thousand, $this->detail->s_decimal);
            if (isNumber($array[$i]->id_amount_novat_conv)==0)
            {
                $array[$i]->id_message.=$and.sprintf(_("Montant incorrect [%s]"),$array[$i]->id_amount_novat);
                $and=",";
            }

            //----------------------------------------------------------------
            // Test for specific filter
            //----------------------------------------------------------------
            switch ($ledger_type)
            {
                case 'ACH':
                    //-----------------
                    ///- Check Service
                    //-----------------
                    $card=Impacc_Verify::check_card($array[$i]->id_acc_second);
                    if ($card==false)
                    {
                         $array[$i]->id_message.=$and.sprintf(_("Fiche inconnue [%s]"),$array[$i]->id_acc_second);
                        
                        $and=",";
                    }
                    Impacc_Csv_Sale_Purchase::check($array[$i], $date_format, $this->detail->s_thousand,
                            $this->detail->s_decimal);
                    break;
                case 'VEN':
                    //-----------------
                    ///- Check Service
                    //-----------------
                    $card=Impacc_Verify::check_card($array[$i]->id_acc_second);
                    if ($card==false)
                    {
                         $array[$i]->id_message.=$and.sprintf(_("Fiche inconnue [%s]"),$array[$i]->id_acc_second);
                        $and=",";
                    }
                    Impacc_Csv_Sale_Purchase::check($array[$i], $date_format, $this->detail->s_thousand,
                            $this->detail->s_decimal);
                    break;
                case 'ODS':
                    // Check that colonne id_debit is C or D
                    if ($array[$i]->id_debit!="D"&&$array[$i]->id_debit!="C")
                    {
                        $array[$i]->id_message.=$and."CK_ERROR_DEBIT";
                        $array[$i]->id_message.=$and.sprintf(_("D ou C [%s]"),$array[$i]->id_debit);
                        $and=",";
                    }
                    break;
                case 'FIN':
                     $card=Impacc_Verify::check_card($array[$i]->id_acc);
                    if ($card==false)
                    {
                        $array[$i]->id_message.=$and.sprintf(_("Fiche inconnue [%s]"),$array[$i]->id_acc);
                        $and=",";
                    }
                    break;
                default :
                    throw new Exception(_('type journal inconnu'));
            }
            // check code devise
            if ($array[$i]->id_currency_code!="")
            {
                $count=$cn->get_value("select count(*) from currency where cr_code_iso=$1",
                        [$array[$i]->id_currency_code]);
                if ($count==0)
                {
                     $array[$i]->id_message.=$and.sprintf(_("Devise inconnue [%s]"),$array[$i]->id_currency_code);
                }
                if ($array[$i]->id_currency_amount=="")
                {
                    $array[$i]->id_currency_amount=0;
                }
                $array[$i]->id_currency_amount=Impacc_Tool::convert_amount($array[$i]->id_currency_amount,
                                $this->detail->s_thousand, 
                                $this->detail->s_decimal);
                if ( isNumber($array[$i]->id_currency_amount) == 0) {
                    $array[$i]->id_message.=$and.sprintf(_("Montant devise incorrect [%s]"),$array[$i]->id_currency_amount);
                }
            }
            // update status
            $array[$i]->update();
        }
        if ($ledger_type=="ODS")
        {
            // Check that D == C for each group
            $array=$cn->get_array("
with deb as (
        select sum(coalesce(id_amount_novat_conv::numeric,0)) as sum_debit,
            id_code_group 
        from 
            impacc2.import_detail 
        where 
            import_id = $1 
            and id_debit='D' 
        group by id_code_group
) ,cred as (
        select sum(coalesce(id_amount_novat_conv::numeric,0)) as sum_credit,
        id_code_group 
        from 
            impacc2.import_detail 
        where 
            import_id = $1 and 
            id_debit='C' 
        group by id_code_group)
        select id_code_group,sum_debit-sum_credit
        from
        deb join cred using(id_code_group)
        where
        sum_debit <> sum_credit
        ", array($p_file->impid));
            $nb_array=count($array);
            for ($e=0; $e<$nb_array; $e++)
            {
                $cn->exec_sql("update impacc2.import_detail set id_message = coalesce(id_message,'')||',CK_BALANCE' where id_code_group=$1 ",
                        array($array[$e]['id_code_group']));
            }
        }
    }





    /// Create the right object for the import id
    /// and throw and exception if the ledger type can not be found
    //\param $p_import_id is the import_file.id
    function make_csv_class($p_import_id)
    {
        $this->load_import($p_import_id);
        $cn=\Dossier::connect();
   
        switch ($this->detail->ledger_type)
        {
            case 'ACH':
                $obj=new Impacc_Csv_Purchase();
                break;
            case 'VEN':
                $obj=new Impacc_Csv_Sale();
                break;
            case 'ODS':
                $obj=new Impacc_Csv_Misc_Operation();
                break;
            case 'FIN':
                $obj=new Impacc_Csv_Bank();
                break;
            default :
                throw new Exception(_('type journal inconnu'));
        }
        $obj->errcode=$this->errcode;
        return $obj;
    }

    /*!
     * \brief Record the given csv file into impacc2.import_detail ,
     * depending of the ledger type a different filter is used to import rows
     * \param $p_file is an Impacc_File , use to open the temporary file
     *
     */
    function record(Impacc_File $p_file)
    {
        try
        {
            $csv_class=$this->make_csv_class($p_file->impid);
            $cn=\Dossier::connect();
            switch ( $this->detail->s_encoding) {
                case 'windows':
                    if ($cn->set_encoding("WIN1252") == -1) {
                        echo "cannot work in win1252";
                    }
                    break;
                case 'latin1':
                    if ($cn->set_encoding("LATIN1") == -1) {
                        echo "cannot work in LATIN1";
                    }
                    break;
            }
            $csv_class->record($this, $p_file);

            $cn->set_encoding("UTF-8");
        }
        catch (Exception $ex)
        {
            record_log($ex->getTraceAsString());
            echo _("Echec dans record")." ".$ex->getMessage();
            throw $ex;
        }
    }

    ///@brief Display result from the table import_detail for CSV import
    /// @see Impacc2_Csv_Bank::record
    /// @see Impacc2_Csv_sale_purchase::record
    /// @see Impacc2_Csv_mis_operation::record
    //!\param $importfile is an Impacc_File object
    function result(Impacc_File $importfile)
    {

        $Detail_CSV=DetailCsv_MTable::build(-1, $importfile->import_file->id);

        printf("<h2>"._("Importation comptabilité n° %s")."</h2>", $importfile->import_file->id);

        $Detail_CSV->display_table(" where import_id = $1 order by id", array($importfile->impid));
        $Detail_CSV->create_js_script();
        Impacc_Tool::margin_bottom();
    }

    /// Transfer the operation to the right ledger
    function transfer_deprecated()
    {
        $cn=\Dossier::connect();
        try
        {
            ///- Create the right object
            $csv_class=$this->make_csv_class($this->detail->import_id);

            ///- Create the ledger object
            $ledger=Impacc_Tool::ledger_factory($this->detail->jrn_def_id);

            ///- Load only the correct group (all the rows in the group must be valid)
            $sql="
                with rejected as ( SELECT distinct id_code_group 
                                    FROM impacc2.import_detail a
                                    where 
                                    import_id=$1
                                    and (id_status != 0 or trim(COALESCE(id_message,'')) !='')
                                    ) 
                select distinct id_code_group ,id_date_format_conv, import_id
                from 
                    impacc2.import_detail 
                where 
                    import_id=$1 
                    and id_code_group not in (select coalesce(id_code_group,'') from rejected)
                
                order by id_date_format_conv asc
                ";

            $array=$cn->get_array($sql, array($this->detail->import_id));
            ///- Call the function insert from a child classs
            $csv_class->insert($array, $ledger);
        }
        catch (Exception $ex)
        {
            record_log($ex->getTraceAsString());
            echo _("Echec dans transfer")." ".$ex->getMessage();
            throw $ex;
        }
    }
    ///@brief Display form to check ACC  file
    ///@param int $id CSV File
    ///@returns HTML string with the form hist
    function check_form()
    {
        $http=new \HttpInput();
        $r="";
        $r.= '<form method="GET" action="do.php"  style="display:inline;position:fixed;bottom:5rem;
background-color: whitesmoke;border:1px solid navy;padding:1rem;">';
        $r.='<p>'._("Refaire la vérification").'</p>';
        $r.= \HtmlInput::hidden("id",$this->get_import_id());
        $r.= \HtmlInput::hidden("ac",$http->request("ac"));
        $r.= \HtmlInput::hidden("plugin_code",$http->request("plugin_code"));
        $r.= \HtmlInput::hidden("sa","hist");
        $r.= \Dossier::hidden();
        $r.= \HtmlInput::submit("check",_("Vérifie"));
        $r.= '</form>';
        return $r;

    }
}
