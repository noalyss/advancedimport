<?php
namespace NoalyssImport;
/*!
 * @file
 * @brief Manage the table with the CSV detail
 *
 */
/*
 * Copyright (C) 2016 Dany De Bontridder <dany@alchimerys.be>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


class DetailCsv_MTable extends \Manage_Table_SQL
{
    /**
     * @brief build the right object
     * @param int $p_id Impacc_Import_detail_SQL.id
     * @param $p_importfileid Impacc_Import_csv_SQL.id
     * @return DetailCsv_Financial_MTable|DetailCsv_Misc_MTable|DetailCsv_Purchase_MTable|DetailCsv_Sale_MTable
     * @throws \Exception
     */
    static function build($p_id,$p_importfileid=null) {
        $cn=\Dossier::connect();
        if ( $p_importfileid != null ) {
            $import_csv_id=$cn->get_value("select id from impacc2.import_csv where import_id=$1",[$p_importfileid]);

            $import_csv=new \Impacc_Import_csv_SQL($cn,$import_csv_id);
        } else {
            $import_csv_id=$cn->get_value("select import_id from impacc2.import_detail where id=$1",[$p_id]);
            $import_csv= new \Impacc_Import_csv_SQL($cn,$import_csv_id);

        }
        $import_detail=new \Impacc_Import_detail_SQL($cn,$p_id);
        switch ($import_csv->getp("ledger_type"))
        {
            case "ACH":
                $obj=new DetailCsv_Purchase_MTable($import_detail);
                break;
            case "VEN":
                $obj=new DetailCsv_Sale_MTable($import_detail);
                break;
            case "ODS":
                $obj=new DetailCsv_Misc_MTable($import_detail);
                break;
            case "FIN":
                $obj=new DetailCsv_Financial_MTable($import_detail);
                break;
            default:
                throw new \Exception("DCM46. unknow");

        }
        $http=new \HttpInput();
        $plugin_code=$http->request("plugin_code");
        $obj->add_json_param("plugin_code", $plugin_code);
        $obj->set_update_row(true);
        $obj->set_append_row(FALSE);
        $obj->set_col_type("id_message","custom");
        $obj->set_col_type("id_quant","numeric");
        $obj->set_col_type("id_amount_novat","numeric");
        $obj->set_col_type("id_currency_amount","numeric");
        $obj->set_col_type("id_currency_code","text");
        return $obj;
    }
    function input_custom($p_key, $p_value)
    {
        switch ($p_key) {
            case 'id_message':
                echo '<pre>'.$p_value."</pre>";
                break;
        }
    }
}