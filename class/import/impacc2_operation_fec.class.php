<?php
namespace NoalyssImport;

/*
 *   This file is part of NOALYSS.
 *
 *   PhpCompta is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   PhpCompta is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with PhpCompta; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2020) Author Dany De Bontridder <danydb@noalyss.eu>

require_once DIR_IMPORT_ACCOUNT.'/class/import/impacc2_operation.class.php';

/**
 * @file
 * @brief 
 */
class Operation_Fec extends Operation
{
 /// Transform a group of rows to an array and set $jr_date_paid
    /// useable by Acc_Ledger_Sale::insert
    function adapt( $p_row)
    {
        bcscale(4);
        $cn=\Dossier::connect();
        
        // Get the code_group
        $code_group=$p_row;
        
        // get all rows from this group
        $sql_row= "
            select id,
                id_amount_novat_conv, 
                id_date_conv,
                id_acc ,
                id_acc_lib,
                id_label,
                id_debit_amount_conv,
                id_acc_second,
                id_acc_second_lib,
                id_pj,
                id_currency_code,
                id_currency_amount
            from 
                impacc2.import_detail
            where
                import_id=$1 
                and id_status=0
                and id_code_group=$2";
        $all_row=$cn->get_array($sql_row,
                array($p_row['import_id'],$p_row['id_code_group']));
        
        // if not data found return
        if (empty($all_row) ) return;
        
        // No block due to analytic
        global $g_parameter;
        $g_parameter->MY_ANALYTIC="N";
        
        // Initialise the array
        $array=array();
         // The first row gives all the information , the others of the group
        // add services
        $array['nb_item']=count($all_row);
        $array['e_comm']=$all_row[0]['id_label'];
        $array['desc']=$all_row[0]['id_label'];
        $array['jrn_concerned']="";
        // Must be transformed into DD.MM.YYYY
        $array['e_pj']=$all_row[0]['id_pj'];
        $array['e_date']=$all_row[0]['id_date_conv'];
        
        $array['mt']=microtime();
        $array['jrn_type']='ODS';
        $array['jr_optype']='NOR';
        
        // ------- Devise -------------
        $currency_code=0;
        $currency_value=$all_row[0]['id_currency_amount'];
        
        $this->set_currency($all_row[0]['id_currency_code'],$currency_code,$currency_value);
        
        $array['p_currency_rate']=$currency_value;
        $array['p_currency_code']=$currency_code;
        
        
        $nb_row=count($all_row);
        for ( $i=0;$i<$nb_row;$i++)
        {
            $credit=$all_row[$i]['id_amount_novat_conv'];
            $debit=$all_row[$i]['id_debit_amount_conv'];
            
            
            if ( $all_row[$i]['id_debit_amount_conv'] != "0") {
                $array["ck".$i]="";
            }
            
            
            $array["poste".$i]=$all_row[$i]["id_acc"].$all_row[$i]["id_acc_second"];
            
            
            $array["amount".$i]=($credit != 0 ) ? $credit : $debit ;
            $array["ld".$i]=($all_row[$i]["id_acc_lib"]=="")?$all_row[$i]['id_acc_second_lib']:$all_row[$i]["id_acc_lib"];
            $array["desc"]=$all_row[$i]["id_label"];
        }
        return $array;
    }
    /// Transfer operation with the status correct to the
    /// accountancy . Change the status of the row (id_status to 1) after
    /// because it can transferred several rows in one operation
    function insert($p_group,\Acc_Ledger $p_ledger)
    {
       $cn=\Dossier::connect();
       $array=$this->adapt($p_group);
       
        //Receipt
        if (trim($array['e_pj']??"")=="") $array["e_pj"]=$p_ledger->guess_pj();
        $array['e_pj_suggest']=$p_ledger->guess_pj();
        
        
        // Insert
        $p_ledger->save($array);

       // Update this group , status = 2  , jr_id
       // and date_payment
        $cn->exec_sql("update impacc2.import_detail set jr_id=$1 , id_status=2 where id_code_group=$2",
                array($p_ledger->jr_id,$p_group['id_code_group']));
       
    }

      /// Record the given csv file into impacc2.import_detail ,
    /// depending of the ledger type a different filter is used to import rows
    ///! \param $p_import is an Import_Fec object
    //! \param $p_file is an Impacc_File , use to open the temporary file
    function record(Impacc_Import $p_import,Impacc_File $p_file)
    {
        try
        {
            // Open the CSV file
            $hFile=fopen($p_file->import_file->i_tmpname, "r");
            $error=0;
            $cn=\Dossier::connect();
            $cn->start();
            $delimiter=$p_import->get_char_delimiter();
            //---- For each row ---
            while ($row=fgetcsv($hFile,0, $delimiter))
            {
                $insert=new \Impacc_Import_detail_SQL($cn);
                $insert->import_id=$p_file->import_file->id;
                
                $nb_row=count($row);
                if ($nb_row<18)
                {
                    $insert->id_status=-1;
                    $insert->id_message=_("Données manquantes $nb_row").iconv('UTF-8', 'UTF-8//TRANSLIT', 
                                                        utf8_encode(join( $delimiter,$row)));
                }
                else
                {
                    $insert->id_ledger_code=$row[0];
                    $insert->id_label=$row[1];
                    $insert->id_code_group=$row[2].'-'.$row[0];
                    $insert->id_date=$row[3];
                    $insert->id_acc=$row[4];
                    $insert->id_acc_lib=iconv('UTF-8', 'UTF-8//TRANSLIT', utf8_encode($row[5]));
                    $insert->id_acc_second=$row[6];
                    $insert->id_acc_second_lib=iconv('UTF-8', 'UTF-8//TRANSLIT', utf8_encode($row[7]));
                    $insert->id_pj=iconv('UTF-8', 'UTF-8//TRANSLIT', utf8_encode($row[8]));
                    $insert->id_pj_date=$row[8];
                    $insert->id_label=iconv('UTF-8', 'UTF-8//TRANSLIT', utf8_encode($row[10]));
                    $insert->id_debit_amount=($row[11]=="")?0:$row[11];
                    $insert->id_amount_novat=($row[12]=="")?0:$row[12];;
                    $insert->id_lettering=$row[13];
                    $insert->id_analytic_ref="";
                    // Currency 
                    $insert->id_currency_code=$row[17];
                    $insert->id_currency_amount=$row[16];
                }
                
                $insert->insert();
            }
            $cn->commit();
        }
        catch (Exception $ex)
        {
            record_log($ex->getTraceAsString());
            $cn->rollback();
            echo _("IMPACCOPFEC-002.Echec dans record")." ".$ex->getMessage();
            throw $ex;
        }
    }
    


}
