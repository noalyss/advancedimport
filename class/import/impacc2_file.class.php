<?php
namespace NoalyssImport;
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/**
 * @file
 * @brief manage the uploaded file for accountancy 
 *
 */
require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_csv.class.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_fec.class.php";
require_once DIR_IMPORT_ACCOUNT."/class/impacc2_operation.class.php";
require_once DIR_IMPORT_ACCOUNT."/database/impacc2_import_file_sql.class.php";
require_once DIR_IMPORT_ACCOUNT."/database/impacc2_import_csv_sql.class.php";

class Impacc_File
{

//--------------------------------------------------
// Will be replaced by Impacc_CSV as soon as the XML
// 
// Import is proposed
//-----------------------------------------------------    

    static $aformat=array(1=>"CSV",2=>"FEC");
    var $format;
    var $filename;
    var $import_file; //!< Impacc_Import_file_SQL $impid
    var $impid; //!< id (PK) of import_file (table : impacc2.import_file)

    /// constructor
    /// @param $p_format string = CSV or FEC
    function __construct($p_format)
    {
        if ( !in_array($p_format,["CSV","FEC"])){
            throw new \Exception(_("IMPACCFILECONSTRUCTOR"));
        }
        $this->format=$p_format;
    }
    
    /// Save the uploaded file and CSV setting if it is CSV import
    function save_file()
    {
        if (trim($_FILES['file_operation']['name']??"")=='')
        {
            alert(_('Pas de fichier donné'));
            return -1;
        }
        
        $this->filename=tempnam($_ENV['TMP'], 'upload_');
        if (!move_uploaded_file($_FILES["file_operation"]["tmp_name"],
                        $this->filename))
        {
            throw new \Exception(_("Fichier non sauvé"), 1);
        }
        $cn=\Dossier::connect();
        $imp=new \Impacc_Import_file_SQL($cn);
        $imp->setp('i_tmpname', $this->filename);
        $imp->setp('i_filename', $_FILES['file_operation']['name']);
        $imp->setp("i_type",$this->format);
        $imp->insert();
        $cn->exec_sql("update impacc2.import_file set i_date_transfer=now() where id=$1",
                array($imp->id));
        $this->import_file=$imp;
        $this->impid=$imp->getp("id");

        try
        {
            $file=Impacc_File::build($this->impid);
            $file->set_import($this->impid);
            $file->set_setting();
            $file->check_setting();
            $file->save_setting();
        }
        catch (\Exception $ex)
        {
            record_log($ex->getMessage());
            record_log($ex->getTraceAsString());
            echo _("Format invalide")," : ",$ex->getMessage();
            throw  $ex;
        }
    }
    
    function load($p_import_id)
    {
        $cn=\Dossier::connect();
        $this->import_file=new \Impacc_Import_file_SQL($cn,$p_import_id);
        $this->impid=$p_import_id;
        
    }
    /**
     * @brief Constructor : construct a object impacc_file and fetch the row from IMPORT_FILE
     *
     * @param int $p_import_id  SQL import_file.id
     * @return \NoalyssImport\Impacc_File
     * @throws Exception invalide type
     */
    static function new_object($p_import_id)
    {
        $cn=\Dossier::connect();
        $import_file=new \Impacc_Import_file_SQL($cn,$p_import_id);
        if ( $import_file->i_type=="CSV")
        {
            $res=new Impacc_File("CSV");
        }elseif ( $import_file->i_type=="FEC") {
            $res=new Impacc_File("FEC");
            
        }else
        {
            throw new \Exception("IMPACCFILE01"._("Type invalide"))   ;
        }
        $res->import_file=$import_file;
        $res->impid=$p_import_id;
        
        return $res;
        
    }
    /**
     * Build a object Impacc_CSV or Impacc_Fec depending of the type , the object Impac_Import is not fetched
     * from the database
     * 
     * @param int $p_import_id  SQL import_file.id
     * @return \NoalyssImport\Impacc_File
     * @throws \Exception invalide type
     */
    static function build($p_import_id)
    {
        $cn=\Dossier::connect();
        $import_file=new \Impacc_Import_file_SQL($cn,$p_import_id);
        if ( $import_file->i_type=="CSV")
        {
            $res=new Impacc_CSV();
        }elseif ( $import_file->i_type=="FEC") {
            $res=new Impacc_FEC();
            
        }else
        {
            throw new \Exception("IMPACCFILE01"._("Type invalide"))   ;
        }
        $res->set_import($p_import_id);
        return $res;
        
    }
    
    /// Load the file in a temporary table
    function record()
    {
        $operation=new Impacc_Operation();
        $operation->record_file($this);
        $cn=\Dossier::connect();
        $cn->exec_sql("update impacc2.import_file set i_date_transfer=now() where id=$1",
                array($this->import_file->id));
    }



    /// Check the rows of the imported file 
    function check()
    {
        $operation=new Impacc_Operation();
        $operation->format=$this->import_file->i_type;
        $operation->check($this);
    }

    /// Display the rows of the imported file and a status for each row
    function result()
    {
        $operation =Impacc_File::build($this->import_file->id);
        $operation->result($this);
    }
    ///@brief Display form to check ACC  file
    ///@param int $id CSV File
    ///@returns HTML string with the form hist
    function form_check($id) {
        $impacc_csv=self::build($id);
        return $impacc_csv->check_form($id);
    }
    /// Show the result of the file import
    function result_transfer()
    {
        
        $cn=\Dossier::connect();
        // Show first the successfully transfered record
        // Show the failed
        $operation=Impacc_File::build($this->import_file->id);
        $operation->load_import($this->import_file->id);

        // Show the target ledger
        $operation->result($this);
    }
    static function get_file()
    {
        $cn=\Dossier::connect();
        $array=$cn->get_array(
                "
                 select   
                 import_file.id,
                 i_filename,
                 i_type,
                 to_char(i_date_transfer,'DD.MM.YY HH24:MI') as stransfer,
                 to_char(i_date_import,'DD.MM.YY HH24:MI') as simport,
                 to_char(i_date_transfer,'YYYYMMDDHHMI') as sorder_transfer,
                 ledger_type
                 from 
                 impacc2.import_file
                 left join impacc2.import_csv on (import_file.id=import_csv.import_id)
                 left join impacc2.import_fec on (import_file.id=import_fec.import_id)
                 order by i_date_transfer desc
                "
                );
        return $array;
    }
    static function get_all_file()
    {
        $cn=\Dossier::connect();
        $array=$cn->get_array(
            "
                 select   
                 import_file.id,
                 i_filename,
                 i_type,
                 to_char(i_date_transfer,'DD.MM.YY HH24:MI') as stransfer,
                 to_char(i_date_import,'DD.MM.YY HH24:MI') as simport,
                 to_char(i_date_transfer,'YYYYMMDDHHMI') as sorder_transfer,
                 ledger_type
                 from 
                 impacc2.import_file
                 left join impacc2.import_csv on (import_file.id=import_csv.import_id)
                 left join impacc2.import_fec on (import_file.id=import_fec.import_id)
                 union 
                 select 
                 	id,
                 	ifa_filename,
                 	'ANC',
                 	 to_char(ifa_date,'DD.MM.YY HH24:MI') ,
                 	'',
	                  to_char(ifa_date,'YYYYMMDDHHMI') as sorder_transfer,
                	''
                 from impacc2.import_file_anc
                 order by 6 desc
                "
        );
        return $array;
    }
    static function display_list()
    {
        $array=Impacc_File::get_all_file();
        require_once DIR_IMPORT_ACCOUNT."/template/history_file.php";
    }
    /// Delete a row in impacc2.import_file
    function delete($id)
    {
        $cn=\Dossier::connect();
        $cn->exec_sql("delete from impacc2.import_file where id=$1",array($id));
        
    }
    static  function select_file()
    {
        $array=Impacc_File::get_file();
        require_once DIR_IMPORT_ACCOUNT."/template/select-acc-file.php";
    }
}

?>
