<?php
namespace NoalyssImport;
/*
 * Copyright (C) 2016 Dany De Bontridder <dany@alchimerys.be>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/* * *
 * @file 
 * @brief
 *
 */
require_once DIR_IMPORT_ACCOUNT.'/class/import/impacc2_csv.class.php';
require_once DIR_IMPORT_ACCOUNT.'/class/import/impacc2_operation.class.php';

class Impacc_Csv_Misc_Operation  extends Operation
{

    //----------------------------------------------
    ///insert file into the table import_detail
    //!\param Impacc_Csv $p_csv CSV setting
    //!\param Impacc_File $p_file File information
    function record(Impacc_Import $p_csv, Impacc_File $p_file)
    {
         global $aseparator;
        // Open the CSV file
        $hFile=fopen($p_file->import_file->i_tmpname, "r");
        $error=0;
        $cn=\Dossier::connect();
        $delimiter=\NoalyssImport\Impacc_Tool::get_char_delimiter($p_csv->detail->s_delimiter);
        
        $s_surround=\NoalyssImport\Impacc_Tool::get_char_surround($p_csv->detail->s_surround);
        $in_char=\NoalyssImport\Impacc_Tool::get_encoding($p_csv->detail->s_encoding);
        //---- For each row ---
        while ($row=fgetcsv($hFile, 0, $delimiter, $s_surround))
        {
            $insert=new \Impacc_Import_detail_SQL($cn);
            $insert->import_id=$p_file->import_file->id;
            $nb_row=count($row);
            if ($nb_row<8)
            {
                 $insert->id_status=-1;
                $insert->id_message=_("Données manquantes").Impacc_Tool::cut_invalid_char(join($delimiter,$row),"UTF-8");
            }
            else
            {
                $insert->id_date=$row[0];
                $insert->id_ledger_code=$row[1];
                $insert->id_code_group=$row[2];
                $insert->id_pj=$row[3];
                $insert->id_acc=$row[4];
                $insert->id_label=$row[5];

                $insert->id_amount_novat=$row[6];
                $insert->id_debit=$row[7];
                // Analytic ref.
                $insert->id_analytic_ref=isset($row[8])?$row[8]:"";
                // Currency (default = 0)
                $insert->id_currency_code=isset($row[9])?$row[9]:"";
                $insert->id_currency_amount=isset($row[10])?$row[10]:"";
            }
            $insert->insert();
        }
    }
    /// Check if Data are valid for one row
    //!@param $row is an Impacc_Import_detail_SQL object
    function check(\Impacc_Import_detail_SQL $row)      
    {
        
    }
   /// Transform a group of rows to an array and set $jr_date_paid
    /// useable by Acc_Ledger_Sale::insert
    function adapt( $p_row)
    {
        bcscale(4);
        $cn=\Dossier::connect();
        
        // Get the code_group
        $code_group=$p_row;
        
        // get all rows from this group
        $sql_row= "
            select id,
                id_amount_novat_conv, 
                id_date_conv,
                (select f_id from fiche_detail where ad_value=id_acc and ad_id=23) as f_id , 
                (select f_id from fiche_detail where ad_value=id_acc and ad_id=1) as fname,
                (select pcm_lib from tmp_pcmn where pcm_val = id_acc) as acc_name,
                id_acc ,
                id_label,
                id_debit,
                id_acc,
                id_pj,
                id_currency_code,
                id_currency_amount,
                id_cardlabel
            from 
                impacc2.import_detail
            where
                import_id=$1 
                and id_code_group=$2";
        $all_row=$cn->get_array($sql_row,
                array($p_row['import_id'],$p_row['id_code_group']));
        // No block due to analytic
        global $g_parameter;
        $g_parameter->MY_ANALYTIC="N";
        
        // Initialise the array
        $array=array();
         // The first row gives all the information , the others of the group
        // add services
        $array['nb_item']=count($all_row);
        $array['e_comm']=$all_row[0]['id_label'];
        $array['desc']=$all_row[0]['id_label'];
        $array['jrn_concerned']="";
        // Must be transformed into DD.MM.YYYY
        $array['e_pj']=$all_row[0]['id_pj'];
        $array['e_date']=$all_row[0]['id_date_conv'];
        
        $array['mt']=microtime();
        $array['jrn_type']='ODS';
        $array['jr_optype']='NOR';
        
        // ------- Devise -------------
        $currency_code=0;
        $currency_value=$all_row[0]['id_currency_amount'];
        
        $this->set_currency($all_row[0]['id_currency_code'],$currency_code,$currency_value);
        
        $array['p_currency_rate']=$currency_value;
        $array['p_currency_code']=$currency_code;
        
        
        $nb_row=count($all_row);
        for ( $i=0;$i<$nb_row;$i++)
        {
            $price=$all_row[$i]['id_amount_novat_conv'];
            if ( $all_row[$i]['id_debit'] == "D") {
                $array["ck".$i]="";
            }
            if ($all_row[$i]['f_id']=="")
            {
                $array["poste".$i]=$all_row[$i]["id_acc"];
                $msg=$all_row[$i]['acc_name'];
            }else {
                $array["qc_".$i]=$all_row[$i]["id_acc"];
                $msg=$all_row[$i]['fname'];
            }
            $array["amount".$i]=$price;
            $array["ld".$i]=(!empty($all_row[$i]["id_cardlabel"]))?$all_row[$i]["id_cardlabel"]:$msg;
        }
        return $array;
    }
    /// Transfer operation with the status correct to the
    /// accountancy . Change the status of the row (id_status to 1) after
    /// because it can transferred several rows in one operation
    function insert($p_group, \Acc_Ledger $p_ledger,$pa_anc)
    {
       $cn=\Dossier::connect();
       $array=array_merge($this->adapt($p_group),$pa_anc);
       
        //Complete the array
        $array["p_jrn"]=$p_ledger->id;
        if ( isset($_POST['hplan'])) $array['hplan']=$_POST['hplan'];
        if ( isset($_POST['pa_id'])) $array['pa_id']=$_POST['pa_id'];
        if ( isset($_POST['op'])) $array['op']=$_POST['op'];
        if ( isset($_POST['val'])) $array['val']=$_POST['val'];
        //Receipt
        if (trim($array['e_pj']??"")=="") $array["e_pj"]=$p_ledger->guess_pj();
        $array['e_pj_suggest']=$p_ledger->guess_pj();

        
        // Insert
        $p_ledger->save($array);

       // Update this group , status = 2  , jr_id
       // and date_payment
        $cn->exec_sql("update impacc2.import_detail set jr_id=$1 , id_status=2 where id_code_group=$2",
                array($p_ledger->jr_id,$p_group['id_code_group']));
       
    }
}
    