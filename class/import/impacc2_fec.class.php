<?php

namespace NoalyssImport;

/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright (C) 2020 Dany De Bontridder <dany@alchimerys.be>
 */
require_once DIR_IMPORT_ACCOUNT."/database/impacc2_import_fec_sql.class.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_operation_fec.class.php";

class Impacc_FEC extends Impacc_Import
{

    function __construct()
    {
        parent::__construct();
        $cn=\Dossier::connect();
        $this->detail=new \Import_fec_SQL($cn);
    }

    function input_format()
    {
        $in_delimiter=new \Iselect("sep_fec");
        $in_delimiter->value=array(
            array("label"=>"tabulation", "value"=>"0"),
            array("label"=>"point-virgule", "value"=>"1")
        );
        $is_missing_account=new \ISelect("missing_fec");
        $is_missing_account->value=array(
            array("label"=>_("non"),"value"=>0),
            array("label"=>_("Oui"),"value"=>1)
        );
        $in_encoding=new \ISelect('in_encoding');
        $in_encoding->value=array(
            array('value'=>"utf-8", 'label'=>_('Unicode')),
            array('value'=>"latin1", 'label'=>_('Latin'))
        );
        $in_encoding->selected=$this->detail->f_encoding;
        require DIR_IMPORT_ACCOUNT.'/template/upload_operation_fec.php';
    }

    /**
     * Transform the delimiter into a char
     */
    function get_char_delimiter()
    {
        if ($this->detail->f_delimiter==0)
            return "\t";
        if ($this->detail->f_delimiter==1)
            return ";";
        throw new \Exception(_("IMPACFEC-001"), _("Delimiteur invalide"));
    }

    //---------------------------------------------------------------------
    ///Get value from post , fill up the Impacc_Import_csv_SQL object
    //---------------------------------------------------------------------

    function set_setting()
    {
        $http=new \HttpInput();
        $this->detail->f_delimiter=$http->post("sep_fec");
        $this->detail->f_cr_mis_acc=$http->post("missing_fec");
        $this->detail->f_encoding=$http->post("in_encoding");
    }

    ///
    /// Check_setting check that setting valid
    //
    function check_setting()
    {
        
    }

    /// Save setting
    function save_setting()
    {
        $this->detail->f_date=date("d.m.Y H:i");
        $this->detail->save();
    }

  
    /// Thank the import_file.id we find the corresponding record from import_csv
    /// and we load id
    //! \param $p_import_id is impacc2.import_file.id
    function load_import($p_import_id)
    {
        try
        {
            $cn=\Dossier::connect();
            $id=$cn->get_value('select id from impacc2.import_fec where import_id=$1', array($p_import_id));
            $this->detail=new \import_fec_SQL($cn, $id);
            $this->detail->load();
        }
        catch (Exception $e)
        {
            echo $e->getMessage();
        }
    }

    /// Check that upload file is correct
    /// Before transfer , we have to check also : 
    ///   # card belong to ledger
    ///   # Periode not closed
    function check(Impacc_File $p_file)
    {
        $this->load_import($p_file->impid);

        // connect to DB
        $cn=\Dossier::connect();

        // Reset status
        $cn->exec_sql("update impacc2.import_detail set id_status =0 ,id_message=null where import_id=$1",
                [$p_file->impid]);


        // load all rows where status != -1
        $t1=new \Impacc_Import_detail_SQL($cn);
        $array=$t1->collect_objects(" where import_id = $1 and coalesce(id_status,0) <> -1 ", array($p_file->impid));
        // for each row check 
        $nb_array=count($array);
        $date_format="Ymd";
        $date_format_sql="YYYYMMDD";

        for ($i=0; $i<$nb_array; $i++)
        {
            $and=($array[$i]->id_message=="")?"":",";
            $array[$i]->id_status=0;
            if (trim($array[$i]->id_code_group??"")=="")
            {
                $array[$i]->id_status=-1;
                $array[$i]->id_message.=$and._(_("Code groupe vide"));
                $and=",";
            }
            //----------------------------------------------
            // Check ledger code
            //----------------------------------------------
            $ledger_actual=$cn->get_value("select j.jrn_def_id 
                                           from 
                                                impacc2.ledger_code as lc
                                                join jrn_def as j on (lc.jrn_def_id=j.jrn_def_id)
                                           where 
                                            lc.ledger_code=$1 
                                            and j.jrn_def_type=$2", [$array[$i]->id_ledger_code, "ODS"]);
            if ($ledger_actual==""||isNumber($ledger_actual)==0)
            {
                $array[$i]->id_message.=$and.sprintf(_("Aucun journal trouvé ou n'est pas un journal OD [%s]"),
                                $array[$i]->id_ledger_code);
                $and=",";
                $ledger_actual=-1;
                $array[$i]->id_status=1;
            }

            //------------------------------------
            //Check date format
            //------------------------------------
            $test=\DateTime::createFromFormat($date_format, $array[$i]->id_date??"");
            if ($test==false)
            {
                $array[$i]->id_status=1;
                $array[$i]->id_message.=$and.sprintf(_("Date malformée [%s]"), $array[$i]->id_date);
                $and=",";
                
            }
            else
            {
                $array[$i]->id_date_conv=$test->format('d.m.Y');
                $array[$i]->id_date_format_conv=$test->format('Ymd');
                // Check if date exist and in a open periode
                $sql=sprintf("select p_id from parm_periode where p_start <= to_date($1,'%s') "
                        ." and p_end >= to_date($1,'%s') ", $date_format_sql, $date_format_sql);
                $periode_id=$cn->get_value($sql, array($array[$i]->id_date));
                if ($cn->size()==0)
                {
                    $array[$i]->id_message.=$and.sprintf(_("Période inexistante pour [%s]"), $array[$i]->id_date_conv);
                    $array[$i]->id_status=1;
                    $and=",";
                }
                else
                {
                    // Check that this periode is open for this ledger
                    $per=new \Periode($cn, $periode_id);
                    $per->jrn_def_id=$ledger_actual;
                    if ($ledger_actual>0&&$per->is_open()==0)
                    {
                        $array[$i]->id_message.=$and.sprintf(_("Période fermée pour [%s]"), $array[$i]->id_date);
                        $and=",";
                        $array[$i]->id_status=1;
                    }
                }
            }

            //---------------------------------------------------------------
            // Check amount
            // --------------------------------------------------------------

            $array[$i]->id_amount_novat_conv=Impacc_Tool::convert_amount($array[$i]->id_amount_novat, 0, 1);

            if (isNumber($array[$i]->id_amount_novat_conv)==0)
            {
                $array[$i]->id_message.=$and.sprintf(_("Montant incorrect [%s]"), $array[$i]->id_amount_novat);
                $and=",";
                $array[$i]->id_amount_novat_conv=0;
                $array[$i]->id_status=1;
            }
            $array[$i]->id_debit_amount_conv=Impacc_Tool::convert_amount($array[$i]->id_debit_amount, 0, 1);
            if (isNumber($array[$i]->id_debit_amount_conv)==0)
            {
                $array[$i]->id_message.=$and.sprintf(_("Montant incorrect [%s]"), $array[$i]->id_debit_amount);
                $and=",";
                $array[$i]->id_debit_amount_conv=0;
                $array[$i]->id_status=1;
            }


            // check code devise
            if ($array[$i]->id_currency_code!="")
            {
                $count=$cn->get_value("select count(*) from currency where cr_code_iso=$1",
                        [$array[$i]->id_currency_code]);
                if ($count==0)
                {
                    $array[$i]->id_message.=$and.sprintf(_("Devise inconnue [%s]"), $array[$i]->id_currency_code);
                    $array[$i]->id_status=1;
                }
                if ($array[$i]->id_currency_amount=="")
                {
                    $array[$i]->id_currency_amount=0;
                }
                $array[$i]->id_currency_amount=Impacc_Tool::convert_amount($array[$i]->id_currency_amount, 0, 1);
                if (isNumber($array[$i]->id_currency_amount)==0)
                {
                    $array[$i]->id_message.=$and.sprintf(_("Montant devise incorrect [%s]"),
                                    $array[$i]->id_currency_amount);
                    $array[$i]->id_status=1;
                }
            }
            // update status
            $array[$i]->update();
        }

       
     
        // Check that D == C for each group
        $this->check_balance($p_file->impid);
        
        // check same date 
        $this->check_same_date($p_file->impid);
        
        if ( $this->detail->f_cr_mis_acc == 0)
        {
            // Check accounting
            $this->check_accounting($p_file->impid,"id_acc");
            // check auxilary accounting
            $this->check_accounting($p_file->impid,"id_acc_second");
        }
        
    }
    /// Check that in the same group the date is always the same
    /// @param $p_import_id is the import_file.id 
    private function check_same_date($p_import_id)
    {
        $cn=\Dossier::connect();
        $message=\sql_string(_("Opération avec dates différentes dans un groupe"));
        $cn->exec_sql("update impacc2.import_detail 
                set id_message=coalesce (id_message,'')||',$message'
                , id_status =1
            where 
            id_code_group in 
	(select  distinct a.id_code_group 
	from 
	impacc2.import_detail  as a
	join impacc2.import_detail as b on (a.id_code_group=b.id_code_group and a.import_id =b.import_id )
	where 
	b.id < a.id
	and  a.id_date <> b.id_date
	and a.import_id =$1
	)
        and import_id=$1",[$p_import_id]);
        
    }
    /// check if operation are balanced for each group
    /// @param $p_import_id is the import_file.id 
    private function check_balance($p_import_id)
    {
        $cn=\Dossier::connect();
        
        $message=\sql_string(_("Balance incorrecte"));
        $array=$cn->get_array("
            update impacc2.import_detail 
                set id_message=coalesce (id_message,'')|| ',$message' ,
                id_status=1
            where 
                id_code_group in (with deb as (
                        select sum(coalesce(id_amount_novat_conv::numeric,0)) as sum_credit,
                            id_code_group 
                        from 
                            impacc2.import_detail 
                        where 
                            import_id = $1
                        group by id_code_group
                ) ,cred as (
                select sum(coalesce(id_debit_amount_conv::numeric,0)) as sum_debit,
                id_code_group 
                from 
                    impacc2.import_detail 
                where 
                    import_id = $1
                group by id_code_group)
                select id_code_group
                from
                deb join cred using(id_code_group)
                where
                sum_debit <> sum_credit)"
            , array($p_import_id));
    }
    
    /// check if accounting exists
    private function check_accounting($p_import_id,$p_column_acc)
    {
        $cn=\Dossier::connect();
        switch ($p_column_acc)
        {
            case "id_acc":
                $column="id_acc";
                break;
            case "id_acc_second":
                $column="id_acc||coalesce(id_acc_second,'')";
                 break;
            default:
                throw new Exception ("\FEC002.Colonne invalide $p_column_acc");
                break;
        }
        $sql=" update impacc2.import_detail set id_message=coalesce(id_message,'')||$1||'['||$column||']'
                    , id_status=1
                where 
                import_id=$2 and
                not exists (select 1 from tmp_pcmn where pcm_val=$column)
                and $p_column_acc is not null
                ";
        $cn->exec_sql($sql,[ _("Poste comptable inconnu"),$p_import_id]);
    }
    /// Display result from the table import_detail for CSV import
    /// @see Impacc2_Csv_Bank::record
    /// @see Impacc2_Csv_sale_purchase::record
    /// @see Impacc2_Csv_mis_operation::record
    //!\param $importfile is an Impacc_File object
    function result(Impacc_File $importfile)
    {
        $cn=\Dossier::connect();
        $http=new \HttpInput();
        $plugin_code=$http->request("plugin_code");
        $import_detail=new \Impacc_Import_detail_SQL($cn);
        $this->load_import($importfile->import_file->id);
        $manage_table=new \Manage_Table_SQL($import_detail);
        $manage_table->add_json_param("plugin_code", $plugin_code);
        $manage_table->set_update_row(FALSE);
        $manage_table->set_append_row(FALSE);
        printf("<h2>"._("Importation comptabilité n° %s")."</h2>", $importfile->import_file->id);

        $manage_table->set_order(array(
            "id_date",
            "id_ledger_code",
            "id_code_group",
            "id_pj",
            "id_acc",
            "id_acc_second",
            "id_label",
            "id_amount_novat",
            "id_debit_amount",
            "id_message"
        ));
        $manage_table->set_col_label("id_date", _("Date"));
        $manage_table->set_col_label("id_ledger_code", _("Journal"));
        $manage_table->set_col_label("id_code_group", _("Code groupe"));
        $manage_table->set_col_label("id_pj", _("Pièce"));
        $manage_table->set_col_label("id_acc", _("Poste"));
        $manage_table->set_col_label("id_acc_second", _("Poste Auxiliaire"));
        $manage_table->set_col_label("id_label", _("Libellé opération"));
        $manage_table->set_col_label("id_debit_amount", _("Débit"));
        $manage_table->set_col_label("id_amount_novat", _("Crébit"));
        
        $manage_table->set_col_label("id_message", _("Résultat"));



        $manage_table->display_table(" where import_id = $1 order by id", array($importfile->impid));
        Impacc_Tool::margin_bottom();
        $manage_table->create_js_script();
    }
    
    function check_ledger()
    {
        $cn=\Dossier::connect();
        $message=\sql_string(_("Aucun journal trouvé ou n'est pas un journal OD")); 
        
        $cn->exec_sql(
        "update impacc2.import_detail 
            set id_status = 1
            ,id_message =coalesce(id_message,'')||'$message'||'['||id_ledger_code ||']' 
        where 
            id_code_group in (
                select id.id_code_group   from
                impacc2.import_detail id  
                where
                id.import_id =32
                and not exists ( select j.jrn_def_id 
                                from 
                                     impacc2.ledger_code as lc
                                     join jrn_def as j on (lc.jrn_def_id=j.jrn_def_id)
                                where 
                                     lc.ledger_code=id.id_ledger_code
                                     and j.jrn_def_type='ODS'
                                 )
                               )",[$p_import_id]);

    }
    
    /// Record the given csv file into impacc2.import_detail ,
    /// depending of the ledger type a different filter is used to import rows
    //! \param $p_file is an Impacc_File , use to open the temporary file
    function record(Impacc_File $p_file)
    {
        try
        {
           $fec_operation=new Operation_Fec();
           $fec_operation->record($this,$p_file);
           $this->clean_space($p_file);
        }
        catch (Exception $ex)
        {
            record_log($ex->getTraceAsString());
            echo _("FEC001.Echec dans record")." ".$ex->getMessage();
            throw $ex;
        }
    }
    /// Clean trailing space
    ///
    function clean_space(Impacc_File $p_file)
    {
        $cn=\Dossier::connect();
        $cn->exec_sql("
            update impacc2.import_detail 
                set id_ledger_code =trim(id_ledger_code )
                    ,id_acc =trim(id_acc)
                    ,id_acc_second =trim(id_acc_second )
                    ,id_date =trim(id_date ) 
                where 
                    import_id =$1;
            ",[$p_file->import_file->id]);
    }
    /// Create accounting for a specific import_id
    static function create_accounting($p_import_id)
    {
        $fec=new Impacc_FEC();
        $fec->load_import($p_import_id);
        
        // if no automatic creating of accounting return
        if ($fec->detail->f_cr_mis_acc == 0) return ;
        
        $cn=\Dossier::connect();
        // Add accounting
        $sql="insert into tmp_pcmn(pcm_val,pcm_lib)
            select distinct 
                id_acc, id_acc_lib 
            from
                impacc2.import_detail id  
            where 
                import_id = $1
                and id_status=0
                and id_acc not in (select pcm_val from tmp_pcmn tp ) 
                and coalesce (id_acc ,'') <> '' ";
        
        $cn->exec_sql($sql,[$p_import_id]);        
        
        // Add auxiliairy accounting
        $sql = "insert into tmp_pcmn(pcm_val,pcm_lib) select distinct 
                    id_acc||id_acc_second,id_acc_second_lib 
                from 
                    impacc2.import_detail id  
                where 
                    import_id = $1
                and coalesce(id_acc_second,'') <> ''
                and coalesce(id_acc_second_lib,'') <> ''
                and id_status=0
                and id_acc||id_acc_second not in (select pcm_val from tmp_pcmn tp )";
        
        $cn->exec_sql($sql,[$p_import_id]);
    }
    ///@brief Display form to check ACC  file
    ///@returns HTML string with the form hist
    function check_form()
    {
        $http=new \HttpInput();
        $r="";
        $r.= '<form method="GET" action="do.php" style="display:inline;position:fixed;bottom:5rem;background-color: whitesmoke;border:1px solid navy;padding:1rem;">';
        $r.='<p>'._("Refaire la vérification");
        $r.= \HtmlInput::hidden("id",$this->get_import_id());
        $r.= \HtmlInput::hidden("ac",$http->request("ac"));
        $r.= \HtmlInput::hidden("plugin_code",$http->request("plugin_code"));
        $r.= \HtmlInput::hidden("sa","hist");
        $r.= \Dossier::hidden();
        $r.= \HtmlInput::submit("check",_("Vérifie"));
        $r.= '</form>';
        return $r;

    }

}
