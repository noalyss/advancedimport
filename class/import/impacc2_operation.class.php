<?php
namespace NoalyssImport;
/*
 *   This file is part of NOALYSS.
 *
 *   Noalyss is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Noalyss is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Noalyss; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2019) Author Dany De Bontridder <danydb@noalyss.eu>

/**
 * @file
 * @brief Interface for Bank , Misc_Operation, Sale_Purchase, Sale and Purchase
 */


interface Operation_interface
{
    /// Transform a row into an array compatible with Acc_Ledger::input
    ///\param $p_array array from the upload files
    function adapt($p_array);
    
    /// Record operations from the CSV or FEC files, rows in the table impacc2.import_detail
    ///\param Impacc_CSV $p_csv contains the data
    ///\param Impacc_File $p_ledger contains the settings of import
    function record(Impacc_Import $p_csv, Impacc_File $p_ledger);
    
}

abstract class Operation implements Operation_interface {
    
    /**
     * @brief Retrieve the currency value and code,
     * @param string $p_code , code iso of the currency
     * @param int $p_code_id id of the currency
     * @param float $p_rate rate of the currency
     * @see Impacc_CSV_Purchase::adapt
     * @see Impacc_CSV_Sale::adapt
     * 
     */
    function set_currency($p_code,& $p_code_id, & $p_rate)
    {
        $cn=\Dossier::connect();
        $use_currency=FALSE;
        if (file_exists( NOALYSS_INCLUDE."/class/acc_currency.class.php"))
        {
            require_once NOALYSS_INCLUDE."/class/acc_currency.class.php";
            $use_currency=TRUE;

        }

        if ($p_code != NULL && $use_currency == TRUE)
        {
            $currency=new \Acc_Currency($cn);
            $currency->get_by_iso($p_code);
            $p_rate=($p_rate>0)?$p_rate:$currency->get_rate();
            $p_code_id=$currency->get_id();
        }
        else
        {
            $p_rate=1;
            $p_code_id=0;
        }
    }
    
}