<?php
namespace NoalyssImport;
/*
 *   This file is part of NOALYSS.
 *
 *   Noalyss is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Noalyss is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Noalyss; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2018) Author Dany De Bontridder <dany@alchimerys.be>

/**
 * @file
 * @brief 
 * @param type $name Descriptionara
 */

namespace AdvancedImport;

require_once DIR_IMPORT_ACCOUNT."/database/impacc2_v_ledger_code_sql.class.php";
require_once DIR_IMPORT_ACCOUNT."/database/impacc2_ledger_code_sql.class.php";
require_once NOALYSS_INCLUDE.'/lib/manage_table_sql.class.php';

class ManageLedgerCode extends \Manage_Table_SQL
{

    function __construct(\V_Ledger_Code_Manage_SQL $vLedgerCode)
    {
        parent::__construct($vLedgerCode);
        $http=new \HttpInput();

        $this->set_property_visible("id", FALSE);
        $this->set_property_visible("jrn_def_name", FALSE);

        $this->set_col_label("jrn_def_id", _("Journal (dans Noalyss)"));
        $this->set_col_label("ledger_code", _("Code (dans le fichier)"));
        $this->set_callback("ajax.php");

        $this->add_json_param("ac", $http->request("ac"));
        $this->add_json_param("plugin_code", $http->request("plugin_code"));
        $a_ledger=$vLedgerCode->cn->make_array("select jrn_def_id,jrn_def_name from jrn_def order by jrn_def_name ");
        $this->set_col_type("jrn_def_id", "select", $a_ledger);
    }
    function check()
    {
        $cn=$this->table->cn;
        // check duplicate
        if ( $cn->get_value("select count(*) from impacc2.ledger_code 
                                where ledger_code=$1 and id !=$2",
                                array( $this->table->ledger_code,$this->table->id)
                            ) != 0
            
        ){
            $this->aerror["ledger_code"]=_("Ce code est déjà utilisé");
            return false;
        }
        return true;
        
    }
   

}
