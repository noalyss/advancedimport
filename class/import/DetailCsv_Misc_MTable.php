<?php
/*
 *   This file is part of NOALYSS.
 *    NOALYSS is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    NOALYSS is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with NOALYSS; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *    Copyright Author Dany De Bontridder danydb@noalyss.eu
 *
 */

namespace NoalyssImport;
/*!
 * @file
 * @brief Manage the table with the CSV detail for Purchase
 *
 */
/*
 * Copyright (C) 2016 Dany De Bontridder <dany@alchimerys.be>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


use Data_SQL;

class DetailCsv_Misc_MTable  extends DetailCsv_MTable
{
    public function __construct(Data_SQL $p_table)
    {
        parent::__construct($p_table);
        $this->set_order(array(
            "id_date",
            "id_ledger_code",
            "id_code_group",
            "id_pj",
            "id_acc",
            "id_cardlabel",
            "id_label",
            "id_amount_novat",
            "id_debit",
            "id_analytic_ref",
            "id_currency_code",
            "id_currency_amount",
            "id_message"
        ));
        $this->set_col_label("id_date", _("Date"));
        $this->set_col_label("id_ledger_code", _("Journal"));
        $this->set_col_label("id_code_group", _("Code groupe"));
        $this->set_col_label("id_pj", _("Pièce"));
        $this->set_col_label("id_acc", _("Poste/QCode"));
        $this->set_col_label("id_cardlabel",_("Libellé poste/qcode"));
        $this->set_col_label("id_label", _("Libellé opération"));
        $this->set_col_label("id_amount_novat", _("Montant"));
        $this->set_col_label("id_debit", _("D/C"));
        $this->set_col_label("id_analytic_ref", _("Ref Analytique"));
        $this->set_col_label("id_message", _("Résultat"));
    }

}