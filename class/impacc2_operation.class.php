<?php
namespace NoalyssImport;
/*
 * Copyright (C) 2016 Dany De Bontridder <dany@alchimerys.be>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/***
 * @file 
 * @brief Redirect to CSV or other format
 *
 */
/// Redirect to CSV or other format
class Impacc_Operation
{
    var $format; //!< format of the file (CSV or FEC)

    /// call the right filter to import operation
    function record_file(Impacc_File $p_file)
    {
        $operation_import=Impacc_File::build($p_file->import_file->id);
        $operation_import->load_import($p_file->import_file->id);
        $operation_import->record($p_file);
    }
    /// call the check and validate import , depending of the format (CSV...)
    function check(Impacc_File $p_file)
    {
        switch ($p_file->format)
        {
            case 'CSV':

                $csv=new Impacc_CSV();
                $csv->check($p_file);   
                break;
            case 'FEC':

                $FEC=new Impacc_FEC();
                $FEC->check($p_file);
                break;

            default:
                break;
        }
    }
    ///Transfer operation from uploaded file to the 
    /// tables of Noalyss
    //!\param $p_file Impacc_File $p_file
   function transfer(Impacc_File $p_file)
   {
       switch($p_file->format) 
       {
           case 'CSV':
               $csv=new Impacc_CSV();
               $obj=$csv->make_csv_class($p_file->import_file->id);
               $obj->transfer();
               break;
           default:
              throw new Exception(_("Non supporté"), 1);

       }
   }
}
