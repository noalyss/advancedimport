begin;

CREATE OR REPLACE FUNCTION  impacc2.t_import_detail_clean_data()
    RETURNS trigger
AS $function$
declare
    /* variable */
begin
    new.id_acc := substring(new.id_acc from 1 for 254);
    new.id_pj := substring(new.id_pj from 1 for 49);
    new.id_acc_second := substring(new.id_acc_second from 1 for 255);
    new.id_ledger_code := substring(new.id_ledger_code from 1 for 30);
    new.tva_code := substring(new.tva_code from 1 for 20);
    new.id_code_group := substring(new.id_code_group from 1 for 50);
    new.id_cardlabel := substring(new.id_cardlabel from 1 for 100);
    new.id_acc := upper(new.id_acc);
    new.id_acc_second := upper(new.id_acc_second);
    return new;
end ;
$function$
LANGUAGE plpgsql;


create trigger import_detail_clean_data before insert
    or update on
    impacc2.import_detail for each row execute function impacc2.t_import_detail_clean_data();


insert into impacc2.version(v_id,v_text) values (8,'Cut the too long col and set account or card in upper case');
commit;