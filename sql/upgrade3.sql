Begin;

alter table impacc2.import_detail add column id_ledger_code varchar(30);
comment on column impacc2.import_detail.id_ledger_code is 'Ledger code, must have a match in table ledeger_code';

CREATE TABLE impacc2.ledger_code (
	id serial NOT NULL,
	jrn_def_id int8 NOT NULL,
	ledger_code varchar(30) NULL,
	CONSTRAINT ledger_code_pkey PRIMARY KEY (id),
	CONSTRAINT u_ledger_code UNIQUE (ledger_code),
	CONSTRAINT ledger_code_jrn_def_id_fkey FOREIGN KEY (jrn_def_id) REFERENCES jrn_def(jrn_def_id) ON UPDATE CASCADE ON DELETE CASCADE
);

create or replace view impacc2.v_ledger_code_manage
  as select jrn_def_name,
                   ledger_code ,
                   jrn_def.jrn_def_id as jrn_def_id,
                   ledger_code.id
                   from 
                   impacc2.ledger_code 
                   join jrn_def using (jrn_def_id)
                   ;

COMMENT ON TABLE impacc2.ledger_code IS 'Match between code ledger in the file and the existing ledger';

ALTER TABLE impacc2.import_detail DROP CONSTRAINT import_detail_jr_id_fkey;
ALTER TABLE impacc2.import_detail ADD CONSTRAINT import_detail_jr_id_fkey FOREIGN KEY (jr_id) REFERENCES jrn(jr_id) ON UPDATE CASCADE ON DELETE set null;

create view impacc2.v_analytic_operation as 
select distinct
            ida_amount,
            ida_message,
            id_message,
            id1.import_id,
            ida1.import_file_anc_id, 
            id_status,
            trim(upper(id1.id_analytic_ref)) as id_analytic_ref,
            id_code_group,
            ida_anc_account
            id_label,
            poste_analytique.pa_id,
            id_amount_novat,
            id1.id as import_detail_id
        from  
           impacc2.import_detail as id1
           join impacc2.import_detail_anc as ida1 on (trim(upper(id1.id_analytic_ref))=trim(upper(ida1.id_analytic_ref)))
           join poste_analytique on (poste_analytique.po_name=trim(upper(ida1.ida_anc_account)))
           join plan_analytique on (plan_analytique.pa_id=poste_analytique.pa_id)
        where
           ida1.ida_message is  null
           and id1.id_message is  null;
comment on view impacc2.v_analytic_operation is 'Analytic operation to transfer';

insert into impacc2.version(v_id,v_text) values (3,'Add ledger code');
commit;