Begin;

drop view impacc2.v_analytic_operation ;

create view impacc2.v_analytic_operation as 
select distinct
            ida_amount,
            ida_message,
            id_message,
            id1.import_id,
            ida1.import_file_anc_id, 
            id_status,
            trim(upper(id1.id_analytic_ref)) as id_analytic_ref,
            id_code_group,
            ida_anc_account ,
            id_label,
            poste_analytique.pa_id,
            id_amount_novat,
            id1.id as import_detail_id
        from  
           impacc2.import_detail as id1
           join impacc2.import_detail_anc as ida1 on (trim(upper(id1.id_analytic_ref))=trim(upper(ida1.id_analytic_ref)))
           join poste_analytique on (poste_analytique.po_name=trim(upper(ida1.ida_anc_account)))
           join plan_analytique on (plan_analytique.pa_id=poste_analytique.pa_id)
        where
           ida1.ida_message is  null
           and id1.id_message is  null;
comment on view impacc2.v_analytic_operation is 'Analytic operation to transfer';

insert into impacc2.version(v_id,v_text) values (4,'Fix bug in view v_analytic_operation');
commit;
