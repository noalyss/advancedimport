begin;
ALTER TABLE impacc2.import_detail ADD id_cardlabel varchar(100) NULL;
COMMENT ON COLUMN impacc2.import_detail.id_cardlabel IS 'Card Label : optional';
insert into impacc2.version(v_id,v_text) values (7,'Add additional label for CARD in CSV');
commit;