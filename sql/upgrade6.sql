begin;
-- drop table impacc2.import_fec;
create table impacc2.import_fec (id bigserial primary key,
                                 f_delimiter integer not null,
                                 import_id int4 not null,
                                 f_date timestamp default now()
);

ALTER TABLE impacc2.import_fec ADD CONSTRAINT f_delimiter_ck CHECK (f_delimiter in (0,1));
ALTER TABLE impacc2.import_fec ADD CONSTRAINT import_fec_import_id_fkey FOREIGN KEY (import_id) REFERENCES impacc2.import_file(id) ON UPDATE CASCADE ON DELETE CASCADE;

comment on table impacc2.import_fec is 'Import in FEC format';
comment on column impacc2.import_fec.f_delimiter is 'separator 0 for comma, 1 for tabs';
comment on column impacc2.import_fec.f_date is 'date of file loading';

COMMENT ON COLUMN impacc2.import_detail.id_amount_novat IS 'Total wo VAT or amount credit for Misc Op';
COMMENT ON COLUMN impacc2.import_detail.id_code_group IS 'Common to the rows of the same acc operation';
ALTER TABLE impacc2.import_detail ADD id_acc_lib text NULL;
COMMENT ON COLUMN impacc2.import_detail.id_acc_lib IS 'Label of the accounting';
ALTER TABLE impacc2.import_detail ADD id_acc_second_lib text NULL;
COMMENT ON COLUMN impacc2.import_detail.id_acc_second_lib IS 'Label of the secundary accounting';
ALTER TABLE impacc2.import_detail ADD id_pj_date varchar(20) NULL;
COMMENT ON COLUMN impacc2.import_detail.id_pj_date IS 'Date of the receipt';
ALTER TABLE impacc2.import_detail ADD id_lettering varchar(50) NULL;
COMMENT ON COLUMN impacc2.import_detail.id_lettering IS 'Lettering code';



ALTER TABLE impacc2.import_detail ADD id_debit_amount varchar(50) NULL;
COMMENT ON COLUMN impacc2.import_detail.id_debit_amount IS 'Amount debit';
ALTER TABLE impacc2.import_detail ADD id_debit_amount_conv varchar(50) NULL;
COMMENT ON COLUMN impacc2.import_detail.id_debit_amount_conv is 'Debit amount converted';

ALTER TABLE impacc2.import_fec ADD f_cr_mis_acc varchar(1) NULL;
COMMENT ON COLUMN impacc2.import_fec.f_cr_mis_acc IS 'Create missing accounting if set to 1';

ALTER TABLE impacc2.import_fec ADD f_encoding varchar(15) NULL;
COMMENT ON COLUMN impacc2.import_fec.f_cr_mis_acc IS 'encoding : latin1 or unicode';


insert into impacc2.version(v_id,v_text) values (6,'Add import in FEC');
commit;