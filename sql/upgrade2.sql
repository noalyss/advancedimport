begin;

alter table impacc2.import_csv drop column jrn_def_id;

alter table impacc2.import_csv add column ledger_type varchar(4);
comment on column impacc2.import_csv.ledger_type is 'Type of the ledger poss.values are ACH, FIN , VEN or ODS';
alter table impacc2.import_csv add constraint ledger_type_ck check (ledger_type in ('ACH','FIN','VEN','ODS'));

alter table impacc2.import_detail add column id_analytic_ref text;
comment on column impacc2.import_detail.id_analytic_ref is 'this code is the link between analytic and accountancy,it works as a FK';

create table impacc2.import_file_anc(
id bigserial primary key,
ifa_decimal char not null,
ifa_thousand char not null,
ifa_surround char ,
ifa_delimiter char not null,
ifa_filename text not null,
ifa_date timestamp default now());

comment on table impacc2.import_file_anc is 'Import ANC with CSV format ';
comment on column impacc2.import_file_anc.ifa_decimal is 'Decimal symbol';
comment on column impacc2.import_file_anc.ifa_surround is 'Surround';
comment on column impacc2.import_file_anc.ifa_thousand is 'Thousand symbol';
comment on column impacc2.import_file_anc.ifa_delimiter is 'Delimit the column in a CSV row';
comment on column impacc2.import_file_anc.ifa_filename is 'name of the uploaded file ';
comment on column impacc2.import_file_anc.ifa_date is 'Date of upload';

create table impacc2.import_detail_anc
(
	id bigserial primary key,
	id_analytic_ref text,
	ida_amount text,
	ida_anc_account text,
	import_file_anc_id bigint references impacc2.import_file_anc(id) on delete cascade,
        ida_message text
	
);

comment on table impacc2.import_detail_anc is 'Import of analytic operation in CSV format';
comment on column impacc2.import_detail_anc.id_analytic_ref is 'this code is the link between analytic and accountancy,it works as a FK';
comment on column impacc2.import_detail_anc.ida_amount is 'Amount ';
comment on column impacc2.import_detail_anc.ida_anc_account is 'Analytic account, it must match poste_analytique.po_name  ';
comment on column impacc2.import_detail_anc.import_file_anc_id is 'fk to import_file_anc id ';
comment on column impacc2.import_detail_anc.ida_message is 'Message and comment';

ALTER TABLE impacc2.import_detail ADD id_currency_amount text NULL;
COMMENT ON COLUMN impacc2.import_detail.id_currency_amount IS 'Amount in currency (optional)' ;

ALTER TABLE impacc2.import_detail ADD id_currency_code text NULL;
COMMENT ON COLUMN impacc2.import_detail.id_currency_code IS 'Currency code' ;
CREATE OR REPLACE FUNCTION isnumeric(text) RETURNS BOOLEAN AS $$
DECLARE x NUMERIC;
BEGIN
    x := $1::NUMERIC;
    RETURN TRUE;
EXCEPTION WHEN others THEN
    RETURN FALSE;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION isdate(text,text) RETURNS BOOLEAN AS $$
DECLARE x timestamp;
BEGIN
    x := to_date($1,$2);
    RETURN TRUE;
EXCEPTION WHEN others THEN
    RETURN FALSE;
END;
$$
LANGUAGE plpgsql;

insert into impacc2.version(v_id,v_text) values (2,'Add devise and analytic');
commit;

