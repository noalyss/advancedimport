CREATE OR REPLACE VIEW impacc2.v_fec_operation
AS SELECT jrn_def.jrn_def_code,
          jrn_def.jrn_def_name,
          jrn.jr_id,
          jrn.jr_date AS ecrituredate,
          jrnx.j_poste,
          tmp_pcmn.pcm_lib,
          jrnx.j_qcode,
          ( SELECT fd.ad_value
            FROM fiche_detail fd
            WHERE fd.f_id = jrnx.f_id AND fd.ad_id = 1) AS f_id_name,
          jrn.jr_pj_number,
          jrn.jr_date AS piecedate,
          jrn.jr_comment,
          CASE
              WHEN jrnx.j_debit = true THEN jrnx.j_montant
              ELSE 0::numeric
              END AS amount_debit,
          CASE
              WHEN jrnx.j_debit = false THEN jrnx.j_montant
              ELSE 0::numeric
              END AS amount_credit,
          ''::text AS lettering,
          NULL AS lettering_date,
          jrn.jr_date AS validate_date,
          COALESCE(operation_currency.oc_amount, 0::numeric) AS oc_amount,
          currency.cr_code_iso,
          jrnx.j_debit
   FROM jrnx
            JOIN jrn ON jrn.jr_grpt_id = jrnx.j_grpt
            JOIN tmp_pcmn ON jrnx.j_poste::text = tmp_pcmn.pcm_val::text
            JOIN jrn_def ON jrn.jr_def_id = jrn_def.jrn_def_id
            JOIN currency ON currency.id = jrn.currency_id
            LEFT JOIN operation_currency ON jrnx.j_id = operation_currency.j_id

-- Permissions

