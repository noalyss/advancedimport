Begin;

drop view impacc2.v_analytic_operation ;

CREATE OR REPLACE VIEW impacc2.v_analytic_operation
AS SELECT DISTINCT ida1.ida_amount,
    ida1.ida_message,
    id1.id_message,
    id1.import_id,
    ida1.import_file_anc_id,
    id1.id_status,
    btrim(upper(id1.id_analytic_ref)) AS id_analytic_ref,
    id1.id_code_group,
    ida1.ida_anc_account,
    id1.id_label,
    poste_analytique.pa_id,
    poste_analytique.po_id,
    id1.id_amount_novat,
    id1.id AS import_detail_id
   FROM impacc2.import_detail id1
     JOIN impacc2.import_detail_anc ida1 ON btrim(upper(id1.id_analytic_ref)) = btrim(upper(ida1.id_analytic_ref))
     JOIN poste_analytique ON poste_analytique.po_name = btrim(upper(ida1.ida_anc_account))
  WHERE ida1.ida_message IS NULL AND id1.id_message IS NULL; 

comment on view impacc2.v_analytic_operation is 'Analytic operation to transfer';

insert into impacc2.version(v_id,v_text) values (5,'view v_analytic_operation add po_id and pa_id');
commit;
