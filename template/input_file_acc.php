<?php
namespace NoalyssImport;
/*
 *   This file is part of Noalyss.
 *
 *   Noalyss is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Noalyss is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Noalyss; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2014) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))     die('Appel direct ne sont pas permis');
$span=sprintf('<span  id="%s" class=" icon" >&#xe824;</span>', uniqid());
$csv_tab=new \Html_Tab("csv_tab",_("Import Opérations comptables CSV".$span));
$fec_tab=new \Html_Tab("fec_tab",_("Import Opérations comptables FEC".$span));
/**
 * @file
 * @brief 
 * First screen for import , ask for the file and the format (CSV , FEC or XML)
 */
$file=new \IFile("file_operation");
ob_start();
?>
<div style="margin-left:5%";>
<form method="POST" enctype="multipart/form-data" onsubmit="waiting_box();return true;" >
    <?php echo \HtmlInput::array_to_hidden(array('gDossier','ac','plugin_code','sa'), $_REQUEST)?>
    <?php echo \HtmlInput::hidden("import_type","CSV");?>

    <div id="csv_div_id" >
    <?php
    $csv=new Impacc_CSV();
    $csv->input_format();
    ?>
    </div>
    <?php
    echo _('Fichier opérations comptables'),
    $file->input();
    ?>
    <p>
<?php
    echo \HtmlInput::submit("upload", _("Sauve"))
?>
        
    </p>
</form>
</div>
<?php

    $csv_tab->set_content(ob_get_contents());
    ob_clean();
    ob_start();
?>
<div style="margin-left:5%";>
<form method="POST" enctype="multipart/form-data" onsubmit="waiting_box();return true;">
    <?php echo \HtmlInput::array_to_hidden(array('gDossier','ac','plugin_code','sa'), $_REQUEST)?>
    <?php echo \HtmlInput::hidden("import_type","FEC");?>

    <div id="fec_div_id" >
        <?php
        $csv=new Impacc_FEC();
        $csv->input_format();
        ?>
    </div>
    <?php
    echo _('Fichier opérations comptables'),
    $file->input();
    ?>
    <p>
    <?php
    echo \HtmlInput::submit("upload", _("Sauve"))
    ?>
        
    </p>
</form>
<div>
<?php
$fec_tab->set_content(ob_get_contents());
ob_clean();
$form = new \Output_Html_Tab();
$form->set_mode("row");
$form->add($csv_tab);
$form->add($fec_tab);
$form->output();
?>
