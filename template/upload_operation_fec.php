<?php
namespace NoalyssImport;
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/**
 * @file
 * @brief upload operation
 * @see Impacc_Operation::input_format
 */
?>
<p>
   <?php echo _("Chargement d'un fichier FEC en CSV ,");?>

</p>

<table>    
<tr>
<td><?php echo _("Séparateur");?> </td>
<TD> <?php echo $in_delimiter->input()?></td>
</tr>
<tr>
<td>
    <?php echo _("Création des postes comptables manquants")?>
</td>
<td>
    <?php echo $is_missing_account->input();?>
</td>
</tr>
<tr>
    <td>
        <?php echo _("Encodage")?>
    </td>
    <td>
        <?php echo $in_encoding->input()?>
    </td>
</tr>
</table>
<p>
    <?=_("Ces fichiers peuvent être créés avec cette extension")?>
    <a class="line" href="https://gitlab.com/noalyss/noalyss-export/-/wikis/home" target="_blank"><?=_("Export")?></a>
</p>
<div style="height:10em"></div>