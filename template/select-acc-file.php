<?php
namespace NoalyssImport;
/*
 *   This file is part of NOALYSS.
 *
 *   Noalyss is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Noalyss is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Noalyss; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2018) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

/**
 * @file
 * @brief display a list of uploaded acc file ;  select one for the transfer
 */
?>
<table class="result">
    <tr>
        <th>
<?= _("n°") ?>
        </th>
        <th>
<?= _("Fichier") ?>
        </th>
        <th>
<?= _("Type") ?>
        </th>
        <th>
<?= _("Date transfert") ?>
        </th>
        <th>
<?= _("Date") ?>
        </th>
    </tr>
<?php
$nb=count($array);
for ($i=0; $i<$nb; $i++):
    $even=($i%2==0)?' even ':' odd ';
    ?>
        <tr class="<?php echo $even; ?>">
            <td>
    <?= \HtmlInput::anchor_action($array[$i]['id'],"set_acc_file({$array[$i]['id']},'{$array[$i]['i_filename']}')" , uniqid(),"line");?>
    
            </td>
            <td>
    <?= $array[$i]['i_filename'] ?>
            </td>
            <td>
    <?php echo h($array[$i]['i_type']) ?> / 
                <?php echo h($array[$i]['ledger_type']) ?>
            </td>
            <td>
    <?php echo h($array[$i]['simport']) ?>
            </td>

            <td sorttable_customkey="<?= $array[$i]['sorder_transfer'] ?>" >
    <?php echo h($array[$i]['stransfer']) ?>
            </td>
            <td>

            </td>
        </tr>


    <?php
endfor;
?>
</table>