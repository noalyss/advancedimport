<?php
namespace NoalyssImport;
/*
 * * Copyright (C) 2016 Dany De Bontridder <dany@alchimerys.be>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * 
 */

/***
 * @var $array array inherited from NoalyssImport\Impacc_File\display_list() array computed in NoalyssImport\Impac_File\get_all_file
 */
$gDossier=\Dossier::id();
$ac=\HtmlInput::default_value_request("ac", "#");
$plugin_code=\HtmlInput::default_value_request("plugin_code", "#");
/**
 * @file
 * @brief  Show list of imported file, called by NoalyssImport\Impacc_File\display_list()
 */


///$array = [import_file.id,i_filename, i_type, stransfer (transfer date), simport (import date), sorder_transfer(transfer date YYYYMMDDHHMI ), ledger_type
/// inherited from NoalyssImport\Impacc_File\display_list() array computed in NoalyssImport\Impac_File\get_all_file

$cnf=htmlspecialchars(_("Confirme Effacement"));
?>
<form method="post" id="hist_import" onsubmit="confirm_box('hist_import','<?=$cnf?>');return false;">
    <?php
    $http=new \HttpInput();
    echo \HtmlInput::array_to_hidden(
            array ("ac",
                "gDossier",
                "plugin_code",
                ),$_REQUEST);
    echo \HtmlInput::hidden("sb","delete")
    ?>
    <?php
    echo \HtmlInput::submit("sb",_("Efface la sélection"));
    ?>

<table class="result sortable">
    <tr>
        <th class="sorttable_sorted_reverse">
            
            <?php echo _("N°")?>
        </th>
        <th>
            <?php echo _("Fichier")?>
        </th>
        <th>
            <?php echo _("Type")?>
        </th>
        <th>
            <?php echo _("Date import")?>
        </th>
        <th>
            <?php echo _("Date téléchargement")?>
        </th>
        <th  class="sorttable_nosort">

        </th>
        <th  class="sorttable_nosort">
            <?php

            echo \ICheckBox::toggle_checkbox("fileidtg","hist_import");
            ?>
        </th>
    </tr>
    <?php
        $nb=count($array);
        for ($i=0;$i<$nb;$i++):
             $even=($i%2==0)?' even ':' odd ';
    ?>
    <tr id="row<?php echo $array[$i]["id"]?>"  class="<?php echo $even;?>">
        <td>
            <?=$array[$i]['id']?>
        </td>
        <td>
            <?php
            $type=($array[$i]['i_type']=='ANC')?"anc":"acc";
            $url="?".http_build_query(
                    array("gDossier"=>$gDossier,
                        "sa"=>"hist",
                        "ac"=>$ac,
                        "id"=>$array[$i]["id"],
                        "type"=>$type
                        )
                    );
            
            ?>
            <a href="<?php echo $url;?>" onclick="waiting_box();return true;"><?php echo $array[$i]['i_filename'];?> </a>
        </td>
        <td>
            <?php echo h($array[$i]['i_type'])?> / 
            <?php echo h($array[$i]['ledger_type'])?>
        </td>
        <td>
            <?php echo h($array[$i]['simport'])?>
        </td>
      
        <td sorttable_customkey="<?=$array[$i]['sorder_transfer']?>" >
            <?php echo h($array[$i]['stransfer'])?>
        </td>
        <td>
              <?php
            if ($array[$i]['i_type'] != 'ANC') {
                echo \Icon_Action::trash(uniqid(), sprintf("history_delete('%s')", $array[$i]['id']));
            }
            ?>
        </td>
        <td>
            <?php
            if ($array[$i]['i_type'] == 'ANC') {
                $checkbox=new \ICheckBox("fileancid[]",$array[$i]["id"]);
            } else {
                $checkbox=new \ICheckBox('fileid[]',$array[$i]["id"]);
            }
            $checkbox->set_range("fileid");
            echo $checkbox->input();
            ?>
        </td>
    </tr>
    <?php endfor; ?>
</table>
    <?php
    echo \HtmlInput::submit("sb",_("Efface la sélection"));
    ?>
</form>
<?php
echo \ICheckBox::javascript_set_range("fileid");
?>