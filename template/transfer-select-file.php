<?php
namespace NoalyssImport;
/*
 *   This file is part of NOALYSS.
 *
 *   Noalyss is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Noalyss is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Noalyss; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2018) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))     die('Appel direct ne sont pas permis');

/**
 * @file
 * @brief let select the file for accountancy
 */
$sa=$http->request("sa");
$plugin_code=$http->request("plugin_code");
$ac=$http->request("ac");
        
?>
<div id="select_file_div" class="inner_box" style="display: none"></div>
<div id="display_detail_div" class="inner_box" style="display: none"></div>

<form method="POST" onsubmit="waiting_box();return true;">
    <div class="myfieldset">
        <p class="text-muted">
             <?=_("Choisissez les imports comptabilité et / ou analytique à intégrer dans Noalyss, les fichiers analytiques sont optionnels.")?>
             <?=_("Une fois, les données intégrées à la comptabilité dans Noalyss, il n'est pas possible d'annuler")?>
             <?=_("Les données analytiques ne peuvent pas être chargées avec un fichier FEC")?>
        </p>
    <INPUT Type="hidden" name="gDossier" value="<?=\Dossier::id()?>">
    <INPUT Type="hidden" name="sa" value="<?=$sa?>">
    <INPUT Type="hidden" name="plugin_code" value="<?=$plugin_code?>">
    <INPUT Type="hidden" name="ac" value="<?=$ac?>">
    <INPUT Type="hidden" name="sb" value="file_selected">
    <INPUT Type="hidden" value="" id="acc_file_h" name="acc_file_h">
    
    <INPUT Type="hidden" value="" id="anc_file_h" name="anc_file_h">
    <h2><?=_("Comptabilité")?></h2>
    <span id="acc_file">
        
    </span>
    <?php echo \HtmlInput::button_action(_("Efface"),"$('acc_file').innerHTML='';"); ?>
    <?php echo \HtmlInput::button_action(_("Choix fichier comptabilité"),"select_acc_file()",null,"smallbutton"); ?>
    <h2><?=_("Analytique")?></h2>
    <ol id="list_anc_file">
        
    </ol>
    <?php
        echo \HtmlInput::button_action(_("Ajout fichier Analytique"),"add_anc_file()",null,"smallbutton");
    ?>
    </div>
    <?php echo \HtmlInput::submit("transfer_file",_("Transférer"))?>
        
</form>