# Description

Cette application est un plugin pour NOAYSS (https://www.noalyss.eu) 
qui permet d'importer des opérations dans la comptabilité.

Pour l'installer , copier les fichiers dans noalyss/include/ext/import-advanced

! ATTENTION ! si vous téléchargez depuis gitlab.com, le nom du répertoire sera incorrect et devra être changé en import-advanced


# Description
This application is a plugin for NOALYSS , it is a modified 
version of the Noalyss plugin import_account

# Purpose
Import Data, accountancy and analytic from flat file into 
a folder with at least the version 7.1 (currency)


