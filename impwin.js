
/* This file is part of NOALYSS and is under GPL see licence.txt */

/// Show the TVA detail and modify it
/// on save go to imd_parameter.inc.php
function tva_parameter_modify(p_tva)
{
    waiting_box();
    new Ajax.Request(
            "ajax.php",
            {
                parameters: {tva_id: p_tva, gDossier: dossier, plugin_code: plugin_code, ac: ac, action: "tva_parameter_modify"},
                method: "get",
                onSuccess: function (req)
                {
                    var obj = {id: 'tva_detail_id', "cssclass": "inner_box", "style": "width:auto;position:fixed;top:35%;left:15%"};
                    add_div(obj);
                    $('tva_detail_id').innerHTML = req.responseText;
                    req.responseText.evalScripts();
                    remove_waiting_box();
                }
            }
    );
}
/// Check that the parameter for VAT Detail are correct before saving
function check_param_tva()
{
    $("tva_id").style.borderColor = "inherit";
    $("tva_code").style.borderColor = "inherit";
    if ($('tva_id').value == "0") {
        $("tva_id").style.borderColor = "red";
        smoke.alert("Erreur");
        return false;
    }
    if ($('tva_code').value == "") {
        smoke.alert("Erreur");
        $("tva_code").style.borderColor = "red";
        return false;
    }
    return true;
}
/// Add a new matching
function tva_parameter_add()
{
    waiting_box();
    new Ajax.Request(
            "ajax.php",
            {
                parameters: { gDossier: dossier, plugin_code: plugin_code, ac: ac, action: "tva_parameter_add"},
                method: "get",
                onSuccess: function (req)
                {
                    var obj = {id: 'tva_detail_id', "cssclass": "inner_box", "style": "width:auto;position:fixed;top:35%;left:15%"};
                    add_div(obj);
                    $('tva_detail_id').innerHTML = req.responseText;
                    req.responseText.evalScripts();
                    remove_waiting_box();
                }
            }
    );
}
/// Delete a TVA Code
function tva_parameter_delete(id)
{
    smoke.confirm("Effacement ?",function (e)
    {
   if ( e) 
   {
       waiting_box();
        new Ajax.Updater($("row"+id),"ajax.php",{
            method:"get",
            parameters:{"pt_id":id,action:"tva_parameter_delete",gDossier: dossier, plugin_code: plugin_code, ac: ac}
        });
        remove_waiting_box();
   }
});
}
/// Delete a file
function history_delete(id)
{
    smoke.confirm("Effacement ?",function (e)
    {
   if ( e) 
   {
       waiting_box();
        new Ajax.Updater($("row"+id),"ajax.php",{
            method:"get",
            parameters:{"id":id,action:"history_delete",gDossier: dossier, plugin_code: plugin_code, ac: ac}
        });
        remove_waiting_box();
   }
});
}

/**
 * Display a list where you can select a acc file 
 * @see transfer-acc-file.php
 * @returns {undefined}
 */
function select_acc_file()
{
    waiting_box();
     new Ajax.Updater($("select_file_div"),"ajax.php",{
            method:"get",
            parameters:{action:"select_acc_file",gDossier: dossier, plugin_code: plugin_code, ac: ac},
            onSuccess:function(){remove_waiting_box();
                $("select_file_div").show();
            }
        });
                   
}
/**
 * Fill the span with the name and number of the selected file
 * @param integer p_id import_csv.id
 * @param string p_name filename
 */
function set_acc_file(p_id,p_name)
{
    $("acc_file_h").value=p_id;
    $("acc_file").innerHTML=p_id+" "+p_name;
    $('select_file_div').hide();
}
/**
 * Display a list which allow you to select and add an ANC file 
 * @see transfer-acc-file.php
 * @returns {undefined}
 */
function add_anc_file()
{
    waiting_box();
     new Ajax.Updater($("select_file_div"),"ajax.php",{
            method:"get",
            parameters:{action:"add_anc_file",gDossier: dossier, plugin_code: plugin_code, ac: ac,selected:$("anc_file_h").value},
            onSuccess:function(){remove_waiting_box();
                $("select_file_div").show();
            }
        });
}
/**
 * Add file to the hidden input anc_file_h in transfer-file.php
 * @param {type} p_id
 * @returns {undefined}
 */
function set_anc_file(p_id)
{
    new Ajax.Updater($("list_anc_file"),"ajax.php",{
       method:"get",
       parameters:{action:"set_anc_file",import_file_anc_id:p_id,gDossier: dossier, plugin_code: plugin_code, ac: ac},
       insertion:"bottom",
       onSuccess:function(){
           $("select_file_div").hide();
           // parse the list and update anc_file_h
           if ( $("anc_file_h").value == "" )
           {
               $("anc_file_h").value+=p_id;
           } else {
               $("anc_file_h").value+=","+p_id;
               
           }
       }
    });
    
    
}
/**
 * Remove an anc_file 
 * @see transfer-acc-file.php
 * @returns {undefined}
 */
function remove_anc_file(p_rowid)
{
    $("list_anc_file").removeChild($("elt"+p_rowid));
    update_anc_file();

}
/**
 * Update the hidden element anc_file_h with the selected file
 */
function update_anc_file()
{
    $("anc_file_h").value="";
    var list_anc_file=document.getElementById("list_anc_file");
    var nb_element=list_anc_file.childElementCount;
     var sep="";
    for (var i=0;i<nb_element;i++)
    {
      $("anc_file_h").value+=sep+list_anc_file.children[i].getAttribute('import_id');
      sep=",";
    }
}
/**
 * Display the content of the selected ACC file
 * @see transfer-acc-file.php
 * @param {type} p_id
 * @returns {undefined}
 */
function detail_acc(p_id)
{
    
}
/**
 * Display the content of the selected ANC file
 * @see transfer-acc-file.php
 * @param {type} p_id
 * @returns {undefined}
 */
function detail_anc(p_id)
{
    
}