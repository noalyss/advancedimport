<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/* !\file
 * \brief main file for importing card
 */

/**
 * \mainpage Import Advanced IMPOP2
 * Importation de fichiers FEC ou CSV , ainsi que des opérations analytiques
 */

/*
 * load javascript
 */ 
require_once 'impacc2_constant.php';
$http=new \HttpInput();
global $version_plugin;
$plugin_code=$http->request("plugin_code");
$version_plugin=\Extension::get_version(__DIR__."/plugin.xml",$plugin_code);;
global $cn;
echo '<div style="float:right" class="nav-level2 nav-pills"><a class="nav-link" id="help" style="display:inline" href="https://gitlab.com/noalyss/advancedimport/-/wikis/home" target="_blank">Aide</a>'.
'<span>version'.$version_plugin.'</span>'.
'</div>';
$cn=\Dossier::connect();


Extension::check_version(7300);
$ac=$http->request("ac");
?>
<script>
    var dossier = "<?php echo \Dossier::id(); ?>";
    var plugin_code = "<?php echo $plugin_code; ?>";
    var ac = "<?php echo $ac; ?>";
    content[1077]="<?php echo _('D = jour , M = mois , Y = année')?>";
</script>
<?php
// Javascript
ob_start();
require_once('impwin.js');
$j=ob_get_contents();
ob_end_clean();
echo create_script($j);


if ( DEBUGIMPORTADV > 0 ) { echo h2("Mode debug import-advanced");}

$url='?'.\Dossier::get().'&plugin_code='.$plugin_code."&ac=".$ac;

$array=array(
    array($url.'&sa=opc', _('Import Compta'), _('Importation d\'opérations comptable'), "opc"),
    array($url.'&sa=opa', _('Import Analytique'), _('Importation d\'opérations analytique'), "opa"),
    array($url.'&sa=trn',_('Intégration'),_("Préparation avant d'intégrer les opếrations"),"trn"),
    array($url.'&sa=hist', _('Historique'), _('Historique importation'), "hist"),
    array($url.'&sa=param', _('Paramètrage'), _('Paramètrage'), "param")
);
$sa=$http->request("sa", "string", "opr");


if ($cn->exist_schema('impacc2')==false)
{
    require_once DIR_IMPORT_ACCOUNT.'/class/install/install_plugin.class.php';

    $iplugn=new \NoalyssImport\Install_Plugin();
    $iplugn->install($cn);
}
if ( VERSION_IMPACC2 > $cn->get_value('select max(v_id)+1 from impacc2.version') ) {
    require_once DIR_IMPORT_ACCOUNT.'/class/install/install_plugin.class.php';

    $iplugn=new \NoalyssImport\Install_Plugin();
    $iplugn->upgrade();
}
echo "</div>";
echo '<div class="menu2">';
echo ShowItem($array, 'H', 'nav-item', 'nav-link', $sa, ' nav nav-pills nav-level2');
echo '</div>';
    
echo '<div class="content" style="padding:10">';

switch ($sa)
{
    case "param":
        require_once DIR_IMPORT_ACCOUNT.'/include/imd_parameter.inc.php';
        exit();
        break;
    case "opa":
        require_once DIR_IMPORT_ACCOUNT.'/include/imd_operation_anc.inc.php';
        exit();
    case "opc":
        require_once DIR_IMPORT_ACCOUNT.'/include/imd_operation_acc.inc.php';
        exit();

    case "hist":
        require_once DIR_IMPORT_ACCOUNT.'/include/imd_history.inc.php';
        exit();
    case 'trn':
        require_once DIR_IMPORT_ACCOUNT.'/include/imd_transfer.inc.php';
    default:
        break;
}
