<?php
namespace NoalyssImport;
/*
 * * Copyright (C) 2016 Dany De Bontridder <dany@alchimerys.be>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * 
 */

define ("DIR_IMPORT_ACCOUNT",__DIR__);

require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_tva.class.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_file.class.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_import_anc_csv.class.php";
require_once NOALYSS_INCLUDE."/lib/http_input.class.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/DetailCsv_MTable.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/DetailCsv_Financial_MTable.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/DetailCsv_Misc_MTable.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/DetailCsv_Purchase_MTable.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/DetailCsv_Sale_MTable.php";

$http=new \HttpInput();
$plugin_code=$http->request("plugin_code");

/**
 * @file
 * @brief Ajax 
 */
$action=$http->request("action", "string","none");
if ( $action == "tva_parameter_modify")
{
    $tva=new Impacc_TVA();
    $tva_id=\HtmlInput::default_value_request("tva_id", 0);
    $tva->display_modify($tva_id);
}
if ( $action == "tva_parameter_add")
{
    $tva=new Impacc_TVA();
    $tva->display_add();
}
if ( $action == "tva_parameter_delete")
{
    $tva=new Impacc_TVA();
    $tva_id=\HtmlInput::default_value_request("pt_id", 0);
    $tva->delete($tva_id);
    echo "";
}
if ( $action == "history_delete")
{
    $id=\HtmlInput::default_value_request("id", 0);
    $hist=Impacc_File::new_object($id);
    $hist->delete($id);
    echo "";
}
/* 
 * Answer to ajax call 
 */
$table=$http->request("table","string","none");

// Accountancy
if ( $table=="impacc2.import_detail") {
    $id=$http->request("p_id","number");
    $ctl=$http->request("ctl");
    $cn=\Dossier::connect();
    if ($action == "delete")
    {
        $import_detail=new \Impacc_Import_detail_SQL($cn,$id);
        $manage_table=new \Manage_Table_SQL($import_detail);
        $manage_table->add_json_param("plugin_code",$plugin_code);
        $manage_table->set_object_name($ctl);
        header('Content-type: text/xml; charset=UTF-8');
    
        echo $manage_table->ajax_delete()->saveXML();
        return;
    } elseif ($action=="input") {
        // find Import file

        $obj=DetailCsv_MTable::build($id);
        $obj->set_object_name($ctl);
        $obj->send_header();
        echo    $obj->ajax_input()->saveXML();
    } elseif ($action == "save") {

        $obj=DetailCsv_MTable::build($id);
        $obj->set_object_name($ctl);
        $obj->send_header();
        echo    $obj->ajax_save()->saveXML();
    } else {
        throw new \Exception("ajax100.unknow action");
    }
}
// Analytic
if ( $table=="impacc2.import_detail_anc") {
    if ($action == "delete") 
    {
        $cn=\Dossier::connect();
        $id=$http->request("p_id","number");
        $ctl=$http->request("ctl");
        
        $import_detail=new \Impacc_Import_detail_anc_SQL($cn,$id);
        
        $manage_table=new \Manage_Table_SQL($import_detail);
        $manage_table->add_json_param("plugin_code",$plugin_code);
        $manage_table->set_object_name($ctl);
        header('Content-type: text/xml; charset=UTF-8');
    
        echo $manage_table->ajax_delete()->saveXML();
        return;
    }
}

// ledger Code
if ( $table == "impacc2.v_ledger_code_manage" )
{
    require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_manageledgercode.class.php";

    $id=$http->request("p_id","number");
    $ctl=$http->request("ctl");
    $ledger_code=new \V_Ledger_Code_Manage_SQL($cn,$id);
    $ledger=new \AdvancedImport\ManageLedgerCode($ledger_code);
    $ledger->set_object_name($ctl);
    
    // Input data
    if ( $action == "input") {
            header('Content-type: text/xml; charset=UTF-8');
            echo $ledger->ajax_input()->saveXML();
            return;
    }
    
    // Save
    if ( $action == "save")
    {
        $ledger_code=new \V_Ledger_Code_Manage_SQL($cn, $id);
        $ledger_code->setp("jrn_def_id",$http->request("jrn_def_id","number"));
        $ledger_code->setp("ledger_code",$http->request("ledger_code"));
        $ledger=new \AdvancedImport\ManageLedgerCode($ledger_code);
        $ledger->set_object_name($ctl);
        header('Content-type: text/xml; charset=UTF-8');
        echo $ledger->ajax_save()->saveXML();
        return;
    }

    if  ($action == "delete")
    {
        header('Content-type: text/xml; charset=UTF-8');
        echo $ledger->ajax_delete()->saveXML();
        return;
    }
}
// Transfer menu
if ($action == "select_acc_file") 
{
    require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_file.class.php";

    echo \HtmlInput::title_box(_("Choix fichier comptabilité"),"select_file_div","hide");
    $array=Impacc_File::select_file();
    return;
}
/**
 * Select files for ANC 
 */
if ($action == "add_anc_file") 
{
    require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_import_anc_csv.class.php";

    echo \HtmlInput::title_box(_("Choix fichiers analytiques"),"select_file_div","hide");
    $ifile=new Impacc_Import_Anc_Csv();
    $selected=$http->get("selected","string","");
    $array=$ifile->select_file($selected);
    return;
}
if ($action == "remove_anc_file") 
{
    return;
}
if ($action == "remove_acc_file") 
{
    return;
}
/**
 * Create an item to add to the element "list_anc_file"
 */
if ( $action == "set_anc_file") 
{
    $id=$http->get("import_file_anc_id","number");
    
    $info=$cn->get_array("select id,ifa_filename from impacc2.import_file_anc where id=$1 ",[$id]);
    echo <<<EOF
    <li id="elt{$info[0]['id']}" import_id="{$info[0]['id']}">
        (id : {$info[0]['id']})
       {$info[0]['ifa_filename']}
EOF;
   echo \Icon_Action::trash(uniqid(),sprintf("remove_anc_file(%d)",$id));
    echo "    </li>";
    return;
}
