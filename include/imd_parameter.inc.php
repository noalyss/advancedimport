<?php
namespace NoalyssImport;
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/**
 * @file
 * @brief Matching between code from Noalyss and TVA code from file
 *
 */

require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_tva.class.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_manageledgercode.class.php";
$http=new \HttpInput();
$tva = new Impacc_TVA();
// If some data are submitted we must save them before displaying the list
$save=\HtmlInput::default_value_post("save", "#");
echo h2(_("TVA"));
////////////////////////////////////////////////////////////
// Save modification 
////////////////////////////////////////////////////////////
if ( $save != "#")
{
    $id=$http->post("pt_id", "string","0");
    
    $tva_code=$http->post("tva_code", "string","");
    
    // We save the VAT
    if (         $tva_code !=""&&          $id!=0         )
    {
        try
        {
            $tva_id=$http->post("tva_id", "number",0);
            
            if ($id<0)
            {
                $tva->insert($tva_id, $tva_code);
            }
            else
            {
                $tva->update($id, $tva_id, $tva_code);
            }
        }
        catch (Exception $ex)
        {
            alert($ex->getMessage());
        }
    }
    
}

$tva->display_list();

/////////////////////////////////////////////////////////////////////////////////////////////
// Ledgers code
/////////////////////////////////////////////////////////////////////////////////////////////

echo h2(_("Code Journaux"));

$ledger_code=new \V_Ledger_Code_Manage_SQL($cn);
$ledger=new \AdvancedImport\ManageLedgerCode($ledger_code);

$ledger->create_js_script();

$ledger->display_table(" order by jrn_def_name ");
