<?php
namespace NoalyssImport;
/*
 *   This file is part of Noalyss.
 *
 *   Noalyss is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Noalyss is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Noalyss; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2014) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

/**
 * @file
 * @brief history of the import file
 */
require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_file.class.php";
global $http;
$id=$http->get("id","number",0);
$type=$http->get("type","string","acc");
if ( $id > 0){
    if ($type == "acc") {
        $impacc_file=Impacc_File::new_object($id);
        $impacc_file->impid=$id;
        $impacc_file->check();
        $impacc_file->result_transfer();
       echo $impacc_file->form_check($id);
    } elseif ($type=="anc") {
        $impanc=new Impacc_Import_Anc_Csv ($id);
        $impanc->result();
    } else {
        throw new \Exception("IH42.Unknow");
    }
    return;
} elseif (isset ($_POST['fileid']) || isset ($_POST['fileancid'])) {
         $a_fileid=$http->post("fileid","array",array());
        $nb_file=count($a_fileid);
        $impacc_file=new Impacc_File("CSV");
        for ( $i=0;$i<$nb_file;$i++) {
            $impacc_file->delete($a_fileid[$i]);
        }
        $a_fileid=$http->post("fileancid","array",array());
        $nb_file=count($a_fileid);

        for ( $i=0;$i<$nb_file;$i++) {
            echo "delete ",$a_fileid[$i];
            $cn->exec_sql("delete from impacc2.import_file_anc where id=$1",[$a_fileid[$i]]);
        }
}
Impacc_File::display_list();

?>
