<?php
namespace NoalyssImport;
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2016 Author Dany De Bontridder dany@alchimerys.be

/* * *
 * @file
 * @brief upload operation
 *
 */
require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_import.class.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_import_anc_csv.class.php";
require_once NOALYSS_INCLUDE."/lib/html_tab.class.php";
require_once NOALYSS_INCLUDE."/lib/output_html_tab.class.php";


/**
 * Upload anc file
 */
if (isset($_POST['upload_anc']))
{
    // 1 - get format
    $anc_import=new Impacc_Import_Anc_Csv();
    $detail=$anc_import->get_detail();
    $detail->ifa_delimiter=$http->post("in_delimiter", "number");
    $detail->ifa_surround=$http->post("in_surround");
    $detail->ifa_decimal=$http->post("in_decimal", "number");
    $detail->ifa_thousand=$http->post("in_thousand", "number");
    $detail->ifa_filename=$_FILES['file_operation']['name'];
    $detail->ifa_date=date('d.m.Y H:i');
    
    $anc_import->set_detail($detail);
    
    // 2 - check format and save it
    $anc_import->check_setting();
    $anc_import->save_setting();

    // 3 -  upload file
    $file=$anc_import->upload_file();
    
    // 4 - record file
    $anc_import->record($file);
    
    // 5 - display result
    printf("<h2> %d %s %s </h2>",
            $anc_import->get_detail()->getp("id"),
            _("Importation"),
            $anc_import->get_detail()->getp("ifa_filename"));
    $anc_import->result();
    
    return;
}

/*
 * If there is no upload , show the tab to upload files
 */
$impacc_Operation=new Impacc_Import();
$impacc_Operation->input_file_anc();
return;
