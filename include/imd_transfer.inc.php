<?php
namespace NoalyssImport;
/*
 *   This file is part of NOALYSS.
 *
 *   Noalyss is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Noalyss is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Noalyss; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2018) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

require_once DIR_IMPORT_ACCOUNT.'/class/impacc2_transfer.class.php';
/**
 * @file
 * @brief Transfer Menu
 */
$http=new \HttpInput();

$action=$http->request("sb", "string", "");

switch ($action)
{
    case "":
        // step 1 Display where we can add file for accountancy and file for analytic
        include DIR_IMPORT_ACCOUNT.'/template/transfer-select-file.php';
        break;
    
    case "file_selected":
        // step 2 Show contains and correct
        $acc_id=$http->request("acc_file_h","string",0);
        if ($acc_id == 0){
            include DIR_IMPORT_ACCOUNT.'/template/transfer-select-file.php';
            break;
        }        
        echo h1(_("Confirmation"));
        $anc_id=$http->request("anc_file_h");
        $transfer=new Impacc_Transfer($acc_id, $anc_id);

        $transfer->confirm();
        break;
        
    case "transfer":
        // step 3 Files are selected we must transfer them
        $acc_id=$http->request("acc_file_h","string",0);
        if ($acc_id == 0){
            include DIR_IMPORT_ACCOUNT.'/template/transfer-select-file.php';
            break;
        }
        $anc_id=$http->request("anc_file_h");
        echo h1(_("Transfert"));
        $transfer=new Impacc_Transfer($acc_id, $anc_id);
        try {
            $transfer->transfer_file();
        } catch (Exception $e)
        {
            echo h2(_("Erreur pour les fichiers"));
            printf( _("Les imports du fichier comptable %s et des fichiers analytiques %s ont échoués"),
                    $acc_id,
                   $anc_id);
            echo '<p>';
            echo $e->getMessage();
            echo '</p>';
            echo '<p>';
            echo $e->getTraceAsString();
            echo '</p>';
            record_log("Error transfert IMPOP2 : ".
                        "Message = ".$e->getMessage().
                        "Trace = ".$e->getTraceAsString());
            
        }
        break;
    
    default:
        echo "invalide action";
}