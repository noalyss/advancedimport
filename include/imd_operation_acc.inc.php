<?php
namespace NoalyssImport;
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2016 Author Dany De Bontridder dany@alchimerys.be

/* * *
 * @file
 * @brief upload operation
 *
 */
require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_import.class.php";
require_once DIR_IMPORT_ACCOUNT."/class/import/impacc2_import_anc_csv.class.php";
require_once NOALYSS_INCLUDE."/lib/html_tab.class.php";
require_once NOALYSS_INCLUDE."/lib/output_html_tab.class.php";


// Upload ACC file
if (isset($_POST['upload']))
{
    $http=new \HttpInput();
    
    // save the file
    $io=new Impacc_File($http->post("import_type"));

    // save info for file + setting, io->impid is the import_id file
    $io->save_file();

    // record the rows of the file into the right table CSV or XML
    $io->record();

    // Basic check
    $io->check();

    // show the result
    $io->result();
    // if acc then propose to recheck
    if ($io->format=='CSV') {
        $csv=new Impacc_CSV();
        $csv->set_import($io->impid); // impid
        echo $csv->check_form();
    }

    return;
}

/*
 * If there is no upload , show the tab to upload files
 */
$impacc_Operation=new Impacc_Import();
$impacc_Operation->input_file_acc();
return;
